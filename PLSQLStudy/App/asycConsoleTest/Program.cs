﻿using PLSQLService;
using FKLogger;
using Xunit;


namespace Program;


sealed class Program
{
    public async static Task Main(string[] args)
    {
        //service
        string connStr = $"User Id=system;Password=franek111;Data Source=localhost;";
        PLSQLServiceManager sqlService = new(connStr);
        FKLoggerServiceManager logger = new();
        UTestService uTestService = new();


        try
        {
            //przeprowadzamy test połączenia poprzez xunit
            await uTestService.TestConnAsync();


            //pobranie stringa z funkcji plsql
            var result = await sqlService.AsyncGetStringFromFunction();
            logger.FKLogger.Info(result);


            //danie podwyżki pracownikowi
            // await sqlService.AsyncGiveRaise(140);
            // logger.FKLogger.Info("an increase for an employee of 130 was assumed");


            //get employees by teamId
            var employees = await sqlService.AsyncGetEmployeesByTeamId(20);
            foreach (var employee in employees)
                logger.FKLogger.Info(employee.ToString());


            //get view
            var JobsRaport = await sqlService.AsyncGetJobsRaport();
            foreach (var job in JobsRaport)
                logger.FKLogger.Info(job.ToString());


            //get str from package function
            var packageStrFunction = await sqlService.AsyncGetStrFromPackageFunction();
            logger.FKLogger.Info(packageStrFunction);


            //transaction test
            await sqlService.AsyncExecuteSqlInTransaction();
        }
        catch (Exception e)
        {
            logger.FKLogger.Fatal($"[{e.Message}\n\n{e.StackTrace}]");
        }finally
        {
            logger.FKLogger.Info("Program finished work");
        }
    }
}