using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using MVCTestApp.Models;
using PLSQLService;
using FKLogger;


namespace MVCTestApp.Controllers;


public class HomeController : Controller
{
    //services
    private readonly FKLoggerServiceManager _loggerService;
    private readonly PLSQLServiceManager _sqlService;


    public HomeController(ILogger<HomeController> logger)
    {
        string connStr = $"User Id=system;Password=franek111;Data Source=localhost;";
        _loggerService = new();
        _sqlService = new(connStr);
    }


    public async Task<IActionResult> Index()
    {
        var EmpList = await _sqlService.AsyncGetEmployeesByTeamId(20);
        var EmpsJobInfo = await _sqlService.AsyncGetEmployeesJobInfo();
        var JobsRaport = await _sqlService.AsyncGetJobsRaport();


        ViewBag.EmpList = EmpList;
        ViewBag.EmpsJobInfo = EmpsJobInfo;
        ViewBag.JobsRaport = JobsRaport;


        return View();
    }


    public IActionResult Privacy()
    {
        return View();
    }
    

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
