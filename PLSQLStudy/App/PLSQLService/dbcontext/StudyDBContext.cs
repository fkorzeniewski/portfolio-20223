using Microsoft.EntityFrameworkCore;


namespace PLSQLService;


public class StudyDBContext : DbContext
{
    //variables
    private readonly string _connStr;


    //dbsets
    public DbSet<Etat> ETATY { get; set; }
    public DbSet<Pracownik> PRACOWNICY { get; set; }
    public DbSet<JobsRaportRow> JOBSRAPORT { get; set; }


    public StudyDBContext(string connStr) =>
        _connStr = connStr;


    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
        optionsBuilder.UseOracle(_connStr);


    protected override void OnModelCreating(ModelBuilder modelBuilder) =>
        modelBuilder.Entity<JobsRaportRow>().HasNoKey().ToView("JOBSRAPORT");
}