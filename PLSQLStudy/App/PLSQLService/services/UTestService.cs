using Xunit;


namespace PLSQLService;


public class UTestService : UTestServiceBase
{
    [Fact]
    public void TestConn() => 
        Assert.True(PLSQLServiceManager.TestConn(out string mess), mess);


    [Fact]
    public async Task TestConnAsync()
    {
        var result = await PLSQLServiceManager.AsyncCheckConn();
        Assert.True(result, "[TestConnAsync] Async connection check failed");
    }
}