namespace PLSQLService;


public class StandardSqlService : SqlServiceBase
{
    public StandardSqlService(StudyDBContext context) : base(context)
    {
    }


    public int GetJobCount(out string mess)
    {
        try
        {
            mess = string.Empty;


            return _context.ETATY.ToList().Count;
        }
        catch (Exception e)
        {
            mess = $"{e.Message}\n{e.StackTrace}";


            return -1;
        }
    }


    public List<Etat> GetJobs(out string mess)
    {
        try
        {
            mess = string.Empty;


            return _context.ETATY.ToList();
        }
        catch (Exception e)
        {
            mess = $"{e.Message}\n{e.StackTrace}";


            return new List<Etat>();
        }
    }


    public List<Pracownik> GetEmployees(out string mess)
    {
        try
        {
            mess = string.Empty;


            return _context.PRACOWNICY.ToList();
        }
        catch (Exception e)
        {
            mess = $"{e.Message}\n{e.StackTrace}";

            return new List<Pracownik>();
        }
    }


    public List<EmployeeJobInfo> GetEmployeesJobInfo(out string mess)
    {
        mess = string.Empty;


        try
        {
            var query =
                from emp in _context.PRACOWNICY
                join job in _context.ETATY on emp.ETAT equals job.NAZWA
                select new EmployeeJobInfo
                {
                    NAZWISKO = emp.NAZWISKO,
                    ETAT = emp.ETAT,
                    MIN = job.PLACA_MIN,
                    MAX = job.PLACA_MAX
                };


            return query.ToList();

        }
        catch (Exception e)
        {
            mess = $"{e.Message}\n\n{e.StackTrace}";


            return new();
        }
    }


    public List<JobsRaportRow> GetJobsRaport() =>
        _context.JOBSRAPORT.ToList();
}