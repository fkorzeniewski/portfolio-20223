using System.Data;
using System.Data.SqlTypes;
using Microsoft.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;

namespace PLSQLService;


public class AsynchronousSqlServicer : SqlServiceBase
{
    public AsynchronousSqlServicer(StudyDBContext context) : base(context)
    {
    }


    public async Task<bool> AsyncCheckConn()
    {
        bool retVal = false;


        await Task.Run(() =>
        {
            if (_context.Database.CanConnect())
                retVal = true;
            else
                retVal = false;
        });


        return retVal;
    }


    public async Task<string> GetStringFromOracleFunction()
    {
        var resultParam = new OracleParameter
        (
            "result",
            OracleDbType.Varchar2,
            ParameterDirection.ReturnValue
        );
        resultParam.Size = 100;
        await _context.Database.ExecuteSqlRawAsync
            ("BEGIN :result := GetTextFromOracle(); END;", resultParam);



        return resultParam.Value.ToString() ?? throw new NullReferenceException("");
    }


    public async Task<string> AsyncGetStringFromPackageFunction()
    {
        var returnParam = new OracleParameter
        {
            ParameterName = "result",
            OracleDbType = OracleDbType.Varchar2,
            Direction = ParameterDirection.ReturnValue,
            Size = 20
        };


        string query = "BEGIN :result := StudyPackage.studyfunction; END;";
        await _context.Database.ExecuteSqlRawAsync(query,returnParam);


        return returnParam.Value.ToString() ?? throw new NullReferenceException();
    }


    public async Task AsyncExecutePackageProcedure() =>
        await _context.Database.ExecuteSqlRawAsync("EXECUTE StudyPackage.studyprocedure;");
    


    public async Task GiveRaise(int pracId)
    {
        Random rdm = new Random();
        int raise = rdm.Next(2, 3);


        var pracIdParam = new OracleParameter
        (
            "prac_id",
            OracleDbType.Int32,
            pracId,
            ParameterDirection.Input
        );
        var multiplierParam = new OracleParameter
        (
            "multiplier",
            OracleDbType.Int32,
            raise,
            ParameterDirection.Input
        );


        string query = "BEGIN GIVERAISE(:prac_id, :multiplier); END;";
        await _context.Database.ExecuteSqlRawAsync(query, pracIdParam, multiplierParam);
    }


    public async Task<List<Pracownik>> GetEmployeesByTeamID(int teamID)
    {
        var retList = await _context.PRACOWNICY.ToListAsync();
        return retList.Where(x => x.ID_ZESP == teamID).ToList();
    }


    public async Task<List<EmployeeJobInfo>> GetEmployeeJobInfos()
    {
        var query =
            from emp in _context.PRACOWNICY
            join job in _context.ETATY on emp.ETAT equals job.NAZWA
            select new EmployeeJobInfo
            {
                NAZWISKO = emp.NAZWISKO,
                ETAT = emp.ETAT,
                MIN = job.PLACA_MIN,
                MAX = job.PLACA_MAX
            };

            
            return await query.ToListAsync();
    }


    public async Task<List<JobsRaportRow>> AsyncGetJobsRaport() =>
        await _context.JOBSRAPORT.ToListAsync();


    public async Task AsyncExecuteSqlInTransaction()
    {
        using (var transaction = await _context.Database.BeginTransactionAsync())
        {
            try
            {
                string quert = "SELECT * FROM PRACOWNICY";
                await _context.Database.ExecuteSqlRawAsync(quert);


                await transaction.CommitAsync();
            }catch(Exception e)
            {
                await transaction.RollbackAsync();
                throw;
            }
        }
    }
}