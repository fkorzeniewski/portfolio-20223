using System.ComponentModel.DataAnnotations;


namespace PLSQLService;


public class Pracownik 
{
    [Key]
    public decimal? ID_PRAC { get; set; }
    public string? NAZWISKO { get; set; }
    public string? ETAT { get; set; }
    public decimal? ID_SZEFA { get; set; }
    public DateTime? ZATRUDNIONY { get; set; }
    public decimal? PLACA_POD { get; set; }
    public decimal? PLACA_DOD { get; set; }
    public decimal? ID_ZESP { get; set; }


    public override string ToString()=>
        Newtonsoft.Json.JsonConvert.SerializeObject(this);
}