namespace PLSQLService;


public class EmployeeJobInfo
{
    public string? NAZWISKO { get; set; }
    public string? ETAT { get; set; }
    public decimal? MIN { get; set; }
    public decimal? MAX { get; set; }
}