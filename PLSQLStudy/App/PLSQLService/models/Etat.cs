using System.ComponentModel.DataAnnotations;


namespace PLSQLService;


public class Etat
{
    [Key]
    public string? NAZWA { get; set; }
    public decimal PLACA_MIN { get; set; }
    public decimal PLACA_MAX { get; set; }
}