using System.ComponentModel.DataAnnotations;


namespace PLSQLService;


public class JobsRaportRow
{
    [Key]
    public string? TEAM { get; set; }
    public string? JOB { get; set; }
    public Decimal? JOB_COUNT { get; set; }


    public override string ToString()=>
        Newtonsoft.Json.JsonConvert.SerializeObject(this);
}