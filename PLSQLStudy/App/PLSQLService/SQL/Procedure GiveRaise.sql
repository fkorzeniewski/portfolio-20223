create or replace NONEDITIONABLE PROCEDURE GiveRaise
(
    p_id in number,
    p_multiplier in NUMBER
) IS v_value NUMBER;
BEGIN        
    SELECT
        PLACA_POD INTO v_value
    FROM
        PRACOWNICY
    WHERE
        ID_PRAC = p_id;


    --now we multiply value ba p_multiplier value
    v_value := v_value * p_multiplier;


    --update table
    UPDATE 
        PRACOWNICY
    SET 
        PLACA_POD = v_value
    WHERE
        ID_PRAC = p_id;


    COMMIT;
END;