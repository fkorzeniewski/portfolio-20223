﻿using System.Threading.Tasks.Dataflow;


namespace PLSQLService;


public class PLSQLServiceManager : PLSQLServiceManagerBase
{
    private readonly StudyDBContext _context;


    //services
    private readonly StandardSqlService _standardSqlService;
    private readonly AsynchronousSqlServicer _asycSqlService;


    public PLSQLServiceManager(string conn) : base(conn)
    {
        _context = new(conn);
        _standardSqlService = new(_context);
        _asycSqlService = new(_context);
    }


    public override bool TestConn(out string mess)
    {
        try
        {
            if (!_context.Database.CanConnect())
            {
                mess = "Connection to DB unsuccessful";
                return false;
            }


            mess = "Connection to DB successful";
            return true;
        }
        catch (Exception e)
        {
            mess = $"{e.Message}\n{e.StackTrace}";
            return false;
        }
    }


    #region  Standard SqlService


    public int GetJobCount(out string mess) =>
        _standardSqlService.GetJobCount(out mess);


    public List<Etat> GetJobs(out string mess) =>
        _standardSqlService.GetJobs(out mess);


    public List<Pracownik> GetEmployees(out string mess) =>
        _standardSqlService.GetEmployees(out mess);


    public List<EmployeeJobInfo> GetEmployeesJobInfo(out string mess) =>
        _standardSqlService.GetEmployeesJobInfo(out mess);


    public List<JobsRaportRow> GetJobsRaport() =>
        _standardSqlService.GetJobsRaport();


    #endregion


    #region AsycService

    
    public async Task<bool> AsyncCheckConn() =>
        await _asycSqlService.AsyncCheckConn();


    public async Task<string> AsyncGetStringFromFunction() =>
        await _asycSqlService.GetStringFromOracleFunction();


    public async Task AsyncGiveRaise(int pracId)=>
        await _asycSqlService.GiveRaise(pracId);


    public async Task<List<Pracownik>> AsyncGetEmployeesByTeamId(int teamId) =>
        await _asycSqlService.GetEmployeesByTeamID(teamId);


    public async Task<List<EmployeeJobInfo>> AsyncGetEmployeesJobInfo() =>
        await _asycSqlService.GetEmployeeJobInfos();


    public async Task<List<JobsRaportRow>> AsyncGetJobsRaport() =>
        await _asycSqlService.AsyncGetJobsRaport();


    public async Task<string> AsyncGetStrFromPackageFunction() =>
        await _asycSqlService.AsyncGetStringFromPackageFunction();


    public async Task AsyncExecutePackageProcedure() =>
        await AsyncExecutePackageProcedure();


    public async Task AsyncExecuteSqlInTransaction() =>
        await _asycSqlService.AsyncExecuteSqlInTransaction();


    #endregion
}
