namespace PLSQLService;


public abstract class SqlServiceBase
{
    protected readonly StudyDBContext _context;


    public SqlServiceBase(StudyDBContext context)=>
        _context = context;
}