namespace PLSQLService;


public abstract class PLSQLServiceManagerBase :IPLSQLServiceManager
{
    protected readonly string _connStr;


    public PLSQLServiceManagerBase(string connStr) =>
        _connStr = connStr;


    public abstract bool TestConn(out string mess);
}