﻿using PLSQLService;
using FKLogger;
using NLog;


namespace Program;


sealed class Program
{
    static void Main(string[] args)
    {
        //services
        string connStr = $"User Id=system;Password=franek111;Data Source=localhost;";
        PLSQLServiceManager sqlService = new(connStr);
        FKLoggerServiceManager loggerService = new();


        //testujemy połączenie
        if (!sqlService.TestConn(out string mess))
        {
            loggerService.FKLogger.Fatal(mess);
            return;
        }
        else
            loggerService.FKLogger.Info(mess);


        //pobieramy ilość etatów
        int count = sqlService.GetJobCount(out mess);
        if (count == -1)
        {
            loggerService.FKLogger.Fatal(mess);
            return;
        }
        else
            loggerService.FKLogger.Info($"Dowloaded {count} jobs");


        //pobieramy i wyświetlamy etaty
        List<Etat> Jobs = sqlService.GetJobs(out mess);
        if (mess != string.Empty)
        {
            loggerService.FKLogger.Fatal(mess);
            return;
        }
        else
        {
            foreach (Etat job in Jobs)
                loggerService.FKLogger.Info($"{job.NAZWA} {job.PLACA_MAX}");
        }


        //pobieramy i wyświetlamy pracowników
        List<Pracownik> Employee = sqlService.GetEmployees(out mess);
        if (mess != string.Empty)
        {
            loggerService.FKLogger.Fatal(mess);
            return;
        }
        else
        {
            foreach (Pracownik emp in Employee)
                loggerService.FKLogger.Info($"{emp.NAZWISKO} ID: [{emp.ID_PRAC}]");
        }


        //pobranie połączonych informacji o pracowniku i etacie
        List<EmployeeJobInfo> EmpJobs = sqlService.GetEmployeesJobInfo(out mess);
        if (mess != string.Empty)
        {
            loggerService.FKLogger.Fatal(mess);
            return;
        }
        else
        {
            foreach (EmployeeJobInfo inf in EmpJobs)
                loggerService.FKLogger.Info
                    ($"{inf.NAZWISKO} Etat: [{inf.ETAT}] Minimalna {inf.MIN} Maksymalna {inf.MAX}");
        }


        //raport zespołów
        var JobsRaport = sqlService.GetJobsRaport();
        foreach (JobsRaportRow job in JobsRaport)
            loggerService.FKLogger.Info($"[{job.TEAM} {job.JOB} {job.JOB_COUNT}]");
        

        loggerService.FKLogger.Info("Program finished work\n\n");
    }
}