--Create a view query
SELECT 
    e.employee_id
    , e.first_name
    , e.last_name
    , e.email
    , j.job_title
    , jh.department_id
    , jh.start_date
    , jh.end_date
FROM
    employees E
    LEFT JOIN JOBS J ON
        e.job_id = j.job_id
    LEFT JOIN JOB_HISTORY JH on
        e.employee_id = jh.employee_id
WHERE
    START_DATE IS NOT NULL
/
--Create view
CREATE VIEW employee_with_job_history
AS
SELECT 
    e.employee_id
    , e.first_name
    , e.last_name
    , e.email
    , j.job_title
    , jh.department_id
    , jh.start_date
    , jh.end_date
FROM
    employees E
    LEFT JOIN JOBS J ON
        e.job_id = j.job_id
    LEFT JOIN JOB_HISTORY JH on
        e.employee_id = jh.employee_id
WHERE
    START_DATE IS NOT NULL
/
select *
from
    employee_with_job_history
/



