/*
    W tym projekcie chciałem stworzyć procedure, która zwróci liczbę pracowników powyżej jakieść średniej płacy.
*/


-- krok pierwszy ułozyłem sobie zapytanie
SELECT
    count(*)
FROM
    employees EMP
    OUTER APPLY
    (
        SELECT 
            AVG(SALARY) SREDNIA
        FROM
            employees EMP_POD
        WHERE
            emp.job_id = emp_pod.job_id
        GROUP BY
            JOB_ID
    )
WHERE
    SREDNIA >= 9000
/


--krok drugi stworzyłem procedurę z parametrem wejściowym i wyjściowym
CREATE OR REPLACE PROCEDURE get_emp_by_avg_salary
(
    l_avg_salary IN INTEGER
    , emp_count OUT INTEGER
)IS
BEGIN


    SELECT
        count(*) INTO emp_count
    FROM
        employees EMP
        OUTER APPLY
        (
            SELECT 
                AVG(SALARY) SREDNIA
            FROM
                employees EMP_POD
            WHERE
                emp.job_id = emp_pod.job_id
            GROUP BY
                JOB_ID
        )
    WHERE
        SREDNIA >= l_avg_salary;
        
        
END get_emp_by_avg_salary;
/

--krok trzeci przetestowałem działanie procedury
SET SERVEROUTPUT ON;
DECLARE
    lAvgSalary INTEGER := 9000;
    oEmpCount INTEGER;
BEGIN
    get_emp_by_avg_salary(lAvgSalary, oEmpCount);
    DBMS_OUTPUT.PUT_LINE('Liczba pracownik�w: ' || oEmpCount);
END;
/
--krok 4 edytujemy paczkę i body paczki i na koniec ją testujemy ;)
create or replace NONEDITIONABLE PACKAGE employees_package AS
    FUNCTION GET_ALL_EMPLOYEES_INFO RETURN employee_info_table;
    PROCEDURE get_emp_count_by_avg_salary(l_avg_salary in integer, emp_count out integer);
END employees_package;
/
create or replace NONEDITIONABLE PACKAGE BODY employees_package AS


    FUNCTION GET_ALL_EMPLOYEES_INFO RETURN EMPLOYEE_INFO_TABLE IS
    BEGIN
        RETURN GET_EMPLOYEES_INFO();
    END GET_ALL_EMPLOYEES_INFO;
    
    
    PROCEDURE get_emp_count_by_avg_salary 
        (l_avg_salary IN INTEGER,emp_count OUT INTEGER) IS
    BEGIN
        get_emp_by_avg_salary(l_avg_salary,emp_count);
    END get_emp_count_by_avg_salary;


END employees_package;
/
DECLARE
    l_avg_salary INTEGER := 5000;  -- Przykładowa wartość dla średniej pensji
    emp_count INTEGER;
BEGIN
    employees_package.get_emp_count_by_avg_salary(l_avg_salary, emp_count);
    DBMS_OUTPUT.PUT_LINE('Liczba pracowników: ' || emp_count);
END;






