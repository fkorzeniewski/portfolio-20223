--create table for json insert
create table JsonTestTable
(
    test_string varchar2(20)
    , test_int number(8,2)
    , test_number number(8,2)
);
/
drop table JsonTestTable;
/
--json'{"test_string": "Sample Text", "test_int": 123, "test_number": 456.78}';
--create procedure
create or replace procedure InsertToJsonTestTable(jsonTeks clob) is
begin
    insert into JsonTestTable(test_string,test_int,test_number)
        VALUES
        (
            json_value(jsonTeks, '$.test_string' returning VARCHAR2),
            json_value(jsonTeks, '$.test_int' returning number),
            json_value(jsonTeks, '$.test_number' returning number)
        );
    
    
    commit;
end;
/
--test procedure
declare 
    jsonStr blob := '{"test_string": "Sample Text", "test_int": 123, "test_number": 456.78}';
begin
    InsertToJsonTestTable(jsonStr);
end;
/

