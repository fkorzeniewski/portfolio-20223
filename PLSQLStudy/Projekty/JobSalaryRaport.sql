-- create a raport query
SELECT 
    d.department_name
    , e.job_id
    , sum(e.salary+ nvl(commission_pct,0)) full_salary
FROM
    EMPLOYEES e
    left join departments d on
        e.department_id = d.department_id
WHERE
    e.department_id is not null
GROUP BY CUBE
    (d.department_name,e.job_id);
/
-- create row type
CREATE OR REPLACE TYPE emp_raport_row AS OBJECT
(
    DEPARTMENT_NAME VARCHAR2(30)
    , JOB_ID VARCHAR2(10)
    , FULL_SALARY NUMBER(8,2)
);
/
-- create table of emp_raport_row
CREATE OR REPLACE TYPE emp_salary_raport_table AS
    TABLE OF emp_raport_row;
/
-- create function
CREATE OR REPLACE FUNCTION get_emp_salary_raport RETURN emp_salary_raport_table IS
    l_emp_salary_raport emp_salary_raport_table := emp_salary_raport_table();
BEGIN
    SELECT
        emp_raport_row(
            d.department_name
            , e.job_id
            , sum(e.salary+ nvl(commission_pct,0))
        ) BULK COLLECT INTO l_emp_salary_raport
    FROM
        EMPLOYEES e
        left join departments d on
            e.department_id = d.department_id
    WHERE
        e.department_id is not null
    GROUP BY CUBE
        (d.department_name,e.job_id);
        
    
    RETURN l_emp_salary_raport;
END;
/
--test function
SELECT *
FROM
    TABLE(get_emp_salary_raport)
/
-- change package
CREATE OR REPLACE
PACKAGE employees_package AS
    FUNCTION GET_ALL_EMPLOYEES_INFO RETURN employee_info_table;
    PROCEDURE get_emp_count_by_avg_salary(l_avg_salary in integer, emp_count out integer);
    FUNCTION FUNCTION_GET_EMP_SALARY_RAPORT RETURN emp_salary_raport_table;
END employees_package;
/
--change body
CREATE OR REPLACE
PACKAGE BODY employees_package AS


    FUNCTION GET_ALL_EMPLOYEES_INFO RETURN EMPLOYEE_INFO_TABLE IS
    BEGIN
        RETURN GET_EMPLOYEES_INFO();
    END GET_ALL_EMPLOYEES_INFO;
    
    
    PROCEDURE get_emp_count_by_avg_salary 
        (l_avg_salary IN INTEGER,emp_count OUT INTEGER) IS
    BEGIN
        get_emp_by_avg_salary(l_avg_salary,emp_count);
    END get_emp_count_by_avg_salary;
    
    
    FUNCTION FUNCTION_GET_EMP_SALARY_RAPORT RETURN emp_salary_raport_table IS
    BEGIN
        RETURN get_emp_salary_raport();
    END FUNCTION_GET_EMP_SALARY_RAPORT;


END employees_package;
/
--test package
SELECT *
FROM
    TABLE(employees_package.FUNCTION_GET_EMP_SALARY_RAPORT);
/









