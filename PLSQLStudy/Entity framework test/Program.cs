﻿using Microsoft.EntityFrameworkCore;


namespace EntityTest;

//główny program
sealed class Program
{
    static void Main()
    {
        AppDbContext appDbContext= new();
        if(appDbContext.Database.CanConnect())
            Console.WriteLine("Success connect to db");
        else
            Console.WriteLine("Failed to connect to db");
    }
}


// DBContext
public class AppDbContext : DbContext
{
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        // Skonfiguruj połączenie do Oracle
        string connStr = $"User Id=system;Password=franek111;Data Source=localhost;";
        optionsBuilder.UseOracle(connStr);
    }
}