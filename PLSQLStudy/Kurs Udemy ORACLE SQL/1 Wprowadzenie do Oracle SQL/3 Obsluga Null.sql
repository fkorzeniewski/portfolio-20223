/*
Funkcja NVL pozwala nam sprawdzić czy wartość w kolumnie jest nullem a jeżeli tak to zamienić jego wartość na inną
NVL(PLACA_DOD,0) sprawdzi czy wartość kolumny PLACA_DOD jest nullem i jeżeli tak to zamieni ją na 0
*/
SELECT 
    NAZWISKO,
    PLACA_POD,
    NVL(PLACA_DOD,0),
    NVL((PLACA_POD+PLACA_DOD),0) PENSJA,
    PLACA_POD + NVL(PLACA_DOD,0) PENSJA_VER_2
FROM
    pracownicy
-- PLACA_DOD IS NULL wyszuka nam rekordy który w kolumnie PLACA_DOD ma wartość null
WHERE
    PLACA_DOD IS NULL