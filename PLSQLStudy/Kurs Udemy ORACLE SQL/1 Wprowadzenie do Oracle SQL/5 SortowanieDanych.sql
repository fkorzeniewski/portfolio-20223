-- SORTUJEMY ZAPYTANIE ALFABETYCZNIE PO NAZWISKU
SELECT *
FROM
    PRACOWNICY
ORDER BY 
    NAZWISKO


-- DESC (DESCENDING) odwaca nam kolejność sortowania. W tym przypadku będzie to nazwisko posortowanie od ostatniej litery alfabetu
SELECT *
FROM
    PRACOWNICY
ORDER BY 
    NAZWISKO DESC


/*
    Tutaj pojawia się ciekawostka. Nie możemy użyć aliasu w klauzuli WHERE ponieważ przez kolejność wykonywania, która prezentuje się tak:
        - FROM
        - WHERE
        - GROUP BY
        - HAVING
        - SELECT 
        - ORDER BY
    możemy zobaczyć że where wykona się przed selectem, w którym to powstaje alias
*/
SELECT 
    NAZWISKO,
    PLACA_POD,
    PLACA_DOD,
    NVL(PLACA_POD,0)+NVL(PLACA_DOD,0) PENSJA
FROM
    PRACOWNICY
WHERE 
    NVL(PLACA_POD,0)+NVL(PLACA_DOD,0) > 1000
ORDER BY
    PENSJA DESC,
    NAZWISKO