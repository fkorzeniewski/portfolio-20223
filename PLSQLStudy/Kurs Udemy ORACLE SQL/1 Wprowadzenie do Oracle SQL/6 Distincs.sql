-- DISTINCT to polecenie które pozwoli nam prędko pozbyć się duplikatów z tabeli
-- nie jest on jednak zalecany jeżeli zależy nam na prędkości zapytania (lepiej posiłkować się 
-- np. group by)
select DISTINCT
    ETAT
from
    PRACOWNICY