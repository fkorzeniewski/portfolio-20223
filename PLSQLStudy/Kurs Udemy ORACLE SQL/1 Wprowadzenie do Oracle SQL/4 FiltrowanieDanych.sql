-- zwracamy rekordy gdzie płaca podstawowa jest pomiędzy 480 a 1000
SELECT 
    NAZWISKO,
    ID_ZESP,
    ETAT,
    PLACA_POD,
    NVL(PLACA_DOD,0),
    PLACA_POD + NVL(PLACA_DOD,0) PENSJA
FROM
    PRACOWNICY
WHERE 
    PLACA_POD BETWEEN 480 AND 1000


-- zwracamy rekordy gdzie płaca podstawowa NIE jest pomiędzy 480 a 1000
SELECT 
    NAZWISKO,
    ID_ZESP,
    ETAT,
    PLACA_POD,
    NVL(PLACA_DOD,0),
    PLACA_POD + NVL(PLACA_DOD,0) PENSJA
FROM
    PRACOWNICY
WHERE 
    PLACA_POD NOT BETWEEN 480 AND 1000


-- OPERATOR IN sprawił że to zapytanie zwróci nam rekordy które w kolumnie etaty mają wartosci 'ASYSTENT' ALBO 'PROFESOR'
SELECT 
    NAZWISKO,
    ID_ZESP,
    ETAT,
    PLACA_POD,
    NVL(PLACA_DOD,0),
    PLACA_POD + NVL(PLACA_DOD,0) PENSJA
FROM
    PRACOWNICY
WHERE 
    ETAT IN ('ASYSTENT','PROFESOR')


-- OPERATOR IN sprawił że to zapytanie zwróci nam rekordy które w kolumnie etaty NIE mają wartosci 'ASYSTENT' ALBO 'PROFESOR'
SELECT 
    NAZWISKO,
    ID_ZESP,
    ETAT,
    PLACA_POD,
    NVL(PLACA_DOD,0),
    PLACA_POD + NVL(PLACA_DOD,0) PENSJA
FROM
    PRACOWNICY
WHERE 
    ETAT NOT IN ('ASYSTENT','PROFESOR')


--OPERATOR LIKE zwróci nam rekordy gdzie wartość PRZYPOMINA jakiś warunek. Jest to dosyć wolny operator więc warto go wykorzystywać przy 
--wyrażeniach regularnych
SELECT 
    NAZWISKO,
    ID_ZESP,
    ETAT,
    PLACA_POD,
    NVL(PLACA_DOD,0),
    PLACA_POD + NVL(PLACA_DOD,0) PENSJA
FROM
    PRACOWNICY
WHERE 
    NAZWISKO LIKE '_O%'
/* 
    TEN ZAPIS WYŚWIETLI NAM REKORDY KTÓRE W KOLUMNIE NAZWOSKO MAJĄ:
        -PIERWSZĄ LITERĘ DOWOLNĄ (OPERATOR _ )
        -DRUGĄ LITERĘ O (WPISANA KONKRETNA LITERA)
        -DOWOLNY CIĄG (OPERATOR %)
*/
