-- wyswietls wszystko z pracowników
select *
from
    pracownicy
/


--wyswietlanie wybranych kolumn
SELECT
    nazwisko,
    etat,
    zatrudniony
FROM
    pracownicy
/


-- przykład zastosowania aliasu
SELECT 
    nazwisko NAZ
FROM
    pracownicy
/


--FILTROWANIE DANYCH


--wyświetlanie pracowników którzy mają w nazwisku litery 'OW'
SELECT *
FROM
    pracownicy
WHERE
    NAZWISKO LIKE '%OW%'
/


-- OPERATOR AND (nazwisko zawiera litery 'OW' ORAZ placa dodatkowa nie jest wartością pustą)
SELECT *
FROM
    pracownicy
WHERE
    NAZWISKO LIKE '%OW%'
    AND placa_dod IS NOT NULL
/


-- OPERATOR OR (nazwisko zawiera litery 'OW' ALBO placa dodatkowa nie jest wartością pustą)
SELECT *
FROM
    pracownicy
WHERE
    NAZWISKO LIKE '%OW%'
    OR placa_dod IS NOT NULL
/


-- WARTO PAMIĘTAĆ ŻE NAJPIER WYKONUJE SIĘ AND A POTEM OR 
SELECT *
FROM
    pracownicy
WHERE
    id_zesp = 10
    AND 
        NAZWISKO LIKE '%OW%'
    OR 
        placa_dod IS NULL
/

-- id zespołu równe 10 i albo nazwisko ma ow albo płaca dodatkowa jest wartością null
SELECT *
FROM
    pracownicy
WHERE
    id_zesp = 10
    AND 
    (
        NAZWISKO LIKE '%OW%'
        OR 
        placa_dod IS NULL
    )
/