/*
    https://docs.oracle.com/middleware/12212/biee/BIVUG/GUID-DE2A646A-2DAB-4D9E-BDDB-3AA4923BF1CE.htm#BILUG684


    Tutaj dowiedziałem się o istniejących funkcjach matematycznych oraz o tym gdzie szukać ich więcej.
    Dodatkowo dowiedziałem się że istniej tabela dual która to pozwala mi wyświetlać np. wyliczenia matematyczne
    bez odwołania się do istniejacych tabel
*/
SELECT
    PLACA_POD
    , ROUND(PLACA_POD*1.5,2) Zaokraglenie --PODNIESIENIE PLACY O 50% I ZAOKRAGLENIE DO 2 MIEJSCA PO PRZECINKU
    , CEIL(PLACA_POD*1.5) ZaokraglenieWGore --PODNIESIENIE PLACY O 50% I ZAOGLAGLENIE W GORE
    , FLOOR(PLACA_POD*1.5) ZaokraglenieWDul --PODNIESIENIE PLACY O 50% I ZAOGLAGLENIE W DOL
    , MOD(PLACA_POD*1.5,500) Modulo --Operacja matematyczna, sprawdzamy reszte z dzielenia 
FROM
    PRACOWNICY;
    
    
SELECT 
    MOD(5,5) --MODULO
    , POWER(2,3) --POTEGI
    , SQRT(1024) --PIERWIASTEK
FROM
    DUAL