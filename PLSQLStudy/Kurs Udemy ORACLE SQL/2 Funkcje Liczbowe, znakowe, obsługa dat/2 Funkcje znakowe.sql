/*
    https://docs.oracle.com/middleware/1221/biee/BIVUG/GUID-BBA975C7-B2C5-4C94-A007-28775680F6A5.htm

    Na tej lekcji dowiedzialem się o różnych funkcjach znakowych jakie są w bazach danych oracle.
    Dowiedziałem się że jest ich wiele więcej i że zawsze mogę odpalić dokumentacje by poszukać nowych.
    Np. w linku powyżej
*/


SELECT 
    NAZWISKO --nazwisko
    , UPPER(NAZWISKO) DoDuzychLiter --zamieni nazwisko na wielkie litery
    , LOWER(NAZWISKO) DoMalychLiter --zamieni nazwisko na małe litery
    , SUBSTR(NAZWISKO,0,3) WytnijLitery --utnie wszystkie litery od 3 do końca
    , REPLACE(NAZWISKO,'A','X') ZamienLitery --zamieni literę 'A' na 'X'
    , REVERSE(NAZWISKO) OdwrocKolejnosc --odwroci kolejnosc nazwiska
    , LENGTH(NAZWISKO) DlugoscNazwiska --zwroci dlugosc nazwiska
    , RPAD(NAZWISKO,12,'-') DopelnienieZPrawej --dopelni ciąg znakiem '-' z prawej strony
    , LPAD(NAZWISKO,12,'-') DopelnienieZLewej--dopełni ciąg znakiem '-' z lewej strony
FROM
    PRACOWNICY