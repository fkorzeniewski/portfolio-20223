-- concat słyży do łączenia kolumn. Warto pamiętać że nie możemy przeciążać tej metody i jeżeli chcemy połączyć 3 kolumny 
-- lub dodać text między kolumnąmi to musimy zagnieździć tą metodę
SELECT 
    CONCAT
    (
        ID_ZESP
        , NAZWISKO
    ) Pracownik
FROM
    PRACOWNICY


SELECT 
    CONCAT
    (
        CONCAT(ID_ZESP,' ')
        , NAZWISKO
    ) Pracownik
FROM
    PRACOWNICY


-- decode to funkcja która przypomina uprosząną klauzule IF 
-- poniżej sprawdzamy czy id_zesp ma wartość [10] jeżeli tak to pierwszy argument jeżeli nie to drugi
SELECT 
    NAZWISKO
    , ID_ZESP
    , DECODE(ID_ZESP,10,'TRUE','FALSE') CzyRowneDziesiec
FROM    
    PRACOWNICY


-- Case pozwala nam w jednej kolumnie zastonować wiele warunków i kreować względem nich zwracaną watość
SELECT 
    ID_PRAC
    , NAZWISKO
    , PLACA_POD
    , CASE
        WHEN PLACA_POD <= 500 THEN 'MAŁA STAWKA'
        WHEN PLACA_POD <= 1000 THEN 'STAWKA'
        WHEN PLACA_POD > 1000 THEN 'Duża STAWKA'
        ELSE '--'
    END CaseStawka
FROM    
    PRACOWNICY
    

SELECT 
    ID_PRAC
    , NAZWISKO
    , PLACA_POD
    , CASE
        WHEN PLACA_POD <= 500 THEN 'MAŁA STAWKA'
        WHEN PLACA_POD <= 1000 THEN 'STAWKA'
        WHEN PLACA_POD > 1000 THEN 'Duża STAWKA'
        ELSE '--'
    END CaseStawka
    , CASE
        WHEN 
            ETAT IN('ASYSTENT','PROFESOR')
            AND
            ID_ZESP = 40 
        THEN '1'
        WHEN ETAT = 'DYREKTOR' THEN '2'
        ELSE '-'
    END CaseEtat
FROM    
    PRACOWNICY