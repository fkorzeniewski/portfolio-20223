/*
    Rozpoczynając sekcje o rankingachi analizach trzeba powiedzieć że większość funkcji tutaj użytych nie będzie optymalna dla 
    szybkiego działania bazy ale też to nie jest ich rola. Mają one nam pomóc wygenerować różne raporty i zestawienia.
*/


SELECT 
    d.department_name
    , e.job_id
    , sum(e.salary+ nvl(commission_pct,0)) full_salary
FROM
    EMPLOYEES e
    left join departments d on
        e.department_id = d.department_id
WHERE
    e.department_id is not null
GROUP BY rollup
    (d.department_name,e.job_id);
/


SELECT 
    d.department_name
    , e.job_id
    , sum(e.salary+ nvl(commission_pct,0)) full_salary
FROM
    EMPLOYEES e
    left join departments d on
        e.department_id = d.department_id
WHERE
    e.department_id is not null
GROUP BY CUBE
    (d.department_name,e.job_id);
/