Rozpoczynając sekcje o rankingachi analizach trzeba powiedzieć że większość funkcji tutaj użytych nie będzie optymalna dla 
szybkiego działania bazy ale też to nie jest ich rola. Mają one nam pomóc wygenerować różne raporty i zestawienia.

# Rollup

```sql
SELECT 
    d.department_name
    , e.job_id
    , sum(e.salary+ nvl(commission_pct,0)) full_salary
FROM
    EMPLOYEES e
    left join departments d on
        e.department_id = d.department_id
WHERE
    e.department_id is not null
GROUP BY rollup
    (d.department_name,e.job_id);
```

<img src="rollup.png" width=400>

### Powyższe zapytanie stworzy nam ładny raport płać pracowników w różnych departamentach. Na początek dostaniemy informacje o tym jaki etat w jakim dziale ma sume płać następnie dostaniemy sumę płac w departamentach (tam gdzie job_id jest nullem). By na sam koniec dostać sumę płać w całej firmie (departament_name i job_id są nullem)

# Cube

```sql
SELECT 
    d.department_name
    , e.job_id
    , sum(e.salary+ nvl(commission_pct,0)) full_salary
FROM
    EMPLOYEES e
    left join departments d on
        e.department_id = d.department_id
WHERE
    e.department_id is not null
GROUP BY CUBE
    (d.department_name,e.job_id);
```

<img src="cube.png">

### Cubie idzie o krok dalej i do naszego raportu doda nam jeszcze podsumowanie zarobków każdego etatu

różnicą miedzy cube a rollup jest taka że cube będzie generował podsumowanie dla WSZYSTKICH możliwych kombinacji kolumn agregacji, rollup natamiast dla wszystkich poziomów hierarchii 