/*
    Czasami chcelibyśmy podzielić duży zestaw danych na mniejsze cześci. Pomogą nam w tym funkcje ntile i width_bucket. Jest to kolejne DELETE
    potężne narzędzie do analizy danych które pozwalają nam na tworzenie zaawansowanych widoków.
*/

--Ten kod podzieli nam zapytanie na 3 równe grupy (chyba że liczba rekordów będzie nieparzysta wtedy oracle rozieli jeden rekord dodatkowo)
SELECT 
    E.first_name
    , E.last_name
    , E.salary
    , NTILE(3) OVER (ORDER BY E.salary DESC) NTILE
FROM
    employees E
WHERE
    E.department_id BETWEEN 10 AND 40
/
-- ntile to funkcja okienkowa więc możemy oczywiście partycjonować w niej danę
SELECT 
    E.first_name
    , E.last_name
    , E.salary
    , E.department_id
    , NTILE(3) OVER (PARTITION BY E.department_id ORDER BY E.salary DESC) NTILE
FROM
    employees E
WHERE
    E.department_id BETWEEN 10 AND 40
/
SELECT 
    E.first_name
    , E.last_name
    , E.salary
    , E.department_id
    , NTILE(3) OVER (PARTITION BY E.department_id ORDER BY E.salary DESC) NTILE
FROM
    employees E
WHERE
    E.department_id BETWEEN 10 AND 40
/
/*
    WIDTH_BUCKET to funkcja która bardzo mi się spodobała. Możemy w niej podzielić zestaw danych na progi, które sami ustalamy.
    Poniższy kod pobiera departamenty i sumę zarobków pracowników w tych departamentach. Następnie dzieli je na progi od 0 do 50 000.
    Funkcja zwróci nam wartości
    (
        0 - jeżeli x < min_value,
        1 do n jeżeli x <= min_value and x >= max_value
        (n+1) jeżeli x > max_value
    )
    gdzie
        - x to wartość sumy płac
        - n to licznik progu
        - min value minimalna wartość progu
        - max_value maksymalna wartość progu
    
    W funkcji WIDTH_BUCKET(SUM(SALARY), 0,50000,4) liczba 4 oznacza liczbę progów jakie ustaliłem sobie dla tego zapytania
*/
SELECT
    E.department_id
    , SUM(SALARY)
    , WIDTH_BUCKET(SUM(SALARY), 0,50000,4) SALARY_RANK
FROM
    employees E
GROUP BY 
    E.department_id
/
    
    
    
    
    
    
    