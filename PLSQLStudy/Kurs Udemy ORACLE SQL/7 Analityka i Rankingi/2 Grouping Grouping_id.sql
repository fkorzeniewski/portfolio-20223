/*
    Obie funkcje przedstawione na tej lekcji pozwalają nam określić z którego grupowania pochodzi dany rekord.
    GRPUPING zwróci nam wartość 1 jeżeli wiersz jest grupowaniem tabeli określionej w argumencie. GROUPING jest
    jednoargumentowy co wyróżnia go od GROUPING_ID, który jest wieloargumentowy. GROUPING_ID zwróci nam liczbę dzięki
    której określimy z którego grupowania pochodzi wiersz (od 0 czyli naturalne grupowanie do n+1 gdzie n to liczba
    argumentów funkcji)
*/


SELECT 
    e.department_id department
    , e.job_id job 
    , sum(salary) sum
    , GROUPING(E.department_id)
    , GROUPING(E.job_id)
FROM
    EMPLOYEES E
WHERE(
    e.department_id is null
)
or(
    e.department_id in (10,20,30)
)
GROUP BY CUBE
    (E.department_id,E.job_id);
/

SELECT 
    e.department_id department
    , e.job_id job 
    , sum(salary) sum
    , GROUPING_ID(E.department_id,E.job_id)
FROM
    EMPLOYEES E
WHERE(
    e.department_id is null
)
or(
    e.department_id in (10,20,30)
)
GROUP BY CUBE
    (E.department_id,E.job_id);