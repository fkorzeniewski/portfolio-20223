/*
    Jeżeli chcemy przeprowadzić kilka grupowań w zapytaniu sql i chcemy zrobić to w czytelny sposób możemy do tego
    użyć grouping sets. Pozwoli nam to osiągnąć w jednym zapytaniu x liczbę grupowań (bez łączenia zapytań
    np. poprzez union)    
*/
SELECT 
    E.department_id
    , E.job_id
    , SUM(E.salary)
FROM
    EMPLOYEES E
WHERE
    E.department_id IN (10,20,30)
GROUP BY
    GROUPING SETS
    (
        (E.department_id,E.job_id)
        , (E.department_id)
        , (E.job_id)
    )
ORDER BY
    E.department_id
/
/*
    Należy tutaj wspomnieć o tym że podczas wielu grupowań może wystąpić zduplikowanie danych. W wychwytywaniu takich
    duplikatów złuży funckcja group_id. Jeżeli zwróci ona 1 to już wiemy że wiesz jest duplikatem.
*/
SELECT 
    E.department_id
    , E.job_id
    , SUM(E.salary)
    , group_id()
FROM
    EMPLOYEES E
WHERE
    E.department_id IN (10,20,30)
GROUP BY
    GROUPING SETS
    (
        (E.department_id,E.job_id)
        , (E.department_id)
        , (E.job_id)        
        , (E.job_id) --Zduplikowane grupowanie
    )
HAVING
    GROUP_ID() = 0
ORDER BY
    E.department_id
/