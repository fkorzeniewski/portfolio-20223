/*
    ROW_NUMBER pozwoli nam wyświetlić numer rekordu naszego zapytania. W teori można użyć je do rankingu jednak jest to nie zalecane
    (W przypadku gdy 2 i 3 miejsce będą miały taką samą wartość to row_number uzna drugą wartość za większą). O wiele lepiej zadziałają tutaj
    poznane już wcześniej funkcje rankingowe. ROW_NUMBER to funkcja okienkowa (Ma klauzule OVER) więc można użyć na niej partycjonowania.
*/

SELECT
    E.first_name
    , E.last_name
    , E.salary
    , E.department_id
    , ROW_NUMBER() OVER (ORDER BY E.salary DESC) ROW_NUMBER
    , ROW_NUMBER() OVER (PARTITION BY E.department_id ORDER BY E.salary DESC) ROW_NUMBER_PARTITION
FROM
    employees E
/
/*
    Poniższe zapytanie zaprezentuje nam działanie hipotetycznego rankingu. Sprowadza się to do tego żę wprowadzamy danę w RANK(x) x=> 'zarobki pracownika'
    i możemy zobaczyć na którym miejscu plasuje się pracownik o takich zarobkach
*/
SELECT 
    RANK(10500) WITHIN GROUP (ORDER BY E.salary DESC) HIPOTETIC
FROM
    employees E
WHERE
    E.department_id BETWEEN 10 AND 40
/
SELECT
    E.first_name
    , E.last_name
    , E.salary
FROM
    employees E
ORDER BY 
    E.salary DESC
/

