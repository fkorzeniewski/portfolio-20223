/*
    Kolejne potężne narzędzie w bazach danych oracle to funkcje rank oraz dense_rank (są to funkcje okienkowe ale autor kursu
    postanowił o nich powiedzieć już teraz przez to jak czesto się z nich korzysta). Rank i dense_rank pozwala nam tworzyć rankingi rekordów.
    Różnią się one miedzy sobą sposobem traktowania pozycji o takich wartościach rank pominie przy duplikatach niektóre wartości w rankingu ale 
    ale zawsze zwróci nam na konieć ilość rekordów. Dense_ranke natomiast nie pominie nigdy kolejnych numerów rankingu ale przez TO
    już nie pokaże nam ilość rekordów na sam koniec.    
*/


SELECT 
    E.first_name
    , E.last_name
    , E.salary
    , RANK() OVER (ORDER BY SALARY) TOP_SALARY
    , DENSE_RANK() OVER (ORDER BY SALARY) TOP_SALARY
FROM
    employees E
/
/*
    To zapytanie zwróci nam błąd. Pokazuje nam to że te funkję to jednak funkcje okienkowe a nie analityczne.
*/
SELECT 
    E.first_name
    , E.last_name
    , E.salary
    , RANK() OVER (ORDER BY SALARY) TOP_SALARY
    , DENSE_RANK() OVER (ORDER BY SALARY) TOP_SALARY
FROM
    employees E
WHERE
    RANK() OVER (ORDER BY SALARY) = 3
/
/*
    To poprawnę uzycie where z funkcjami rank i dense rank. Trzeba tutaj użyć podzapytania
*/
WITH SALARY_RANKING AS
(
    SELECT 
        E.first_name
        , E.last_name
        , E.salary
        , RANK() OVER (ORDER BY SALARY) TOP_SALARY
        , DENSE_RANK() OVER (ORDER BY SALARY) TOP_SALARY_DENSE
    FROM
        employees E
)
SELECT *
FROM
    SALARY_RANKING
WHERE
    TOP_SALARY_DENSE = 3
/
/*
    Tutaj kolejne potężnie narzędzie czyli partycje. Ten kod zwróci nam ranking płac pracowników w POSZCZEGULNYCH departamentach
*/
SELECT 
    E.first_name
    , E.last_name
    , E.department_id
    , E.salary
    , DENSE_RANK() OVER (PARTITION BY E.department_id ORDER BY E.salary DESC)
FROM
    employees E
/

















