/*
    FETCH FIRST/NEXT to zestaw funkcji które pozwalają nam ograniczyć ilość wyświetlanych rekordów.
    Działają one na liczba ale również na procentach.
*/


--zapytanie zwróci pierwsze 10 wierszy
SELECT 
    *
FROM
    employees E
FETCH FIRST 10 ROWS ONLY
/
--możemy naprzemiennie pisać fetch first albo fetch next
SELECT 
    *
FROM
    employees E
FETCH NEXT 10 ROWS ONLY
/
--użycie procentów
SELECT 
    *
FROM
    employees E
FETCH NEXT 20 PERCENT ROWS ONLY
/
-- offset pozwala nam pominąć np. pierwsze 10 wierszy (nie można pomijać procentami)
SELECT 
    *
FROM
    employees E
OFFSET 10 ROWS
FETCH NEXT 20 PERCENT ROWS ONLY
/
-- offset i fetch next to funkcje a nie klauzuly zapisujemy je zawsze pod koniec zapytania
SELECT 
    *
FROM
    employees E
WHERE
    E.department_id > 20
ORDER BY E.last_name
OFFSET 10 ROWS
FETCH NEXT 20 PERCENT ROWS ONLY
/