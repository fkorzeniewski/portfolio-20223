/*
    Funkcje CUME_DIST i PERCENT_RANK pozwalają nam określić ile wartości w tabeli jest mniejszych (bądź mniejszych równych) aktualnej wartości.
    Obie te funkcje się tym zajmóją jednak jest mała różnica. CUME_DIST weźmie pod uwagę aktualny wiersz a percent_rank już nie
*/


WITH T1 AS
(
    SELECT 
        100 VALUE
    FROM
        DUAL
    UNION
    SELECT 
        200
    FROM
        DUAL
    UNION
    SELECT 
        300
    FROM
        DUAL
    UNION
    SELECT 
        400
    FROM
        DUAL
)
SELECT
    VALUE
    , CUME_DIST() OVER (ORDER BY VALUE) CUME_DIST
    , ROUND(PERCENT_RANK() OVER (ORDER BY VALUE),2) PERCENT_RANK
FROM
    T1

