-- generated as identity będzie sam przydzielał wartości do pola id od 1 w górę
CREATE TABLE TABELA_AUTOMATYCZNA
(
    ID INTEGER GENERATED AS IDENTITY
    , aut_nazwa VARCHAR2(20) DEFAULT 'nazwa' NOT NULL
    , CONSTRAINT TABELA_AUTOMATYCZNA_PK
        PRIMARY KEY(ID)
);
/
/*
    Jeżeli będziemy zwiększali ilość wpisów w tej tabeli a potem je usuniemy to nasza sekwecja będzie lecieć dalej.
    Czyli tak jak poniżej jeżeli dodamy 20 wpisów po czym usuniemy 19 ostatnich to następny będzie miał id = 21 mimo że będzie
    drubim rekordem
*/
INSERT INTO tabela_automatyczna(aut_nazwa) VALUES('qwerty');
/
SELECT *
FROM
    tabela_automatyczna
/
INSERT INTO tabela_automatyczna(ID,aut_nazwa) VALUES(1,'qwerty');--nie przejdzie zapytanie przez sekwencje
/
DELETE FROM tabela_automatyczna WHERE ID != 1;
/
-- możemy manipulować autoincrementem jak poniżej, zacząć od liczby 10 i zwiększać o 2
CREATE TABLE TABELA_AUTOMATYCZNA1
(
    ID INTEGER GENERATED AS IDENTITY(START WITH 10 INCREMENT BY 2)
    , aut_nazwa1 VARCHAR2(20) DEFAULT 'nazwa' NOT NULL
    , CONSTRAINT TABELA_AUTOMATYCZNA1_PK
        PRIMARY KEY(ID)
);
/
--możemy również stworzyć sekwecje i wykorzystać ją do budowania tabel
CREATE SEQUENCE SEQ_TAB_AUT3 INCREMENT BY 1 START WITH 1;
/
CREATE TABLE TABELA_AUTOMATYCZNA2
(
    ID INTEGER DEFAULT seq_tab_aut3.nextval
    , aut_nazwa2 VARCHAR2(20) DEFAULT 'nazwa' NOT NULL
    , CONSTRAINT TABELA_AUTOMATYCZNA2_PK
        PRIMARY KEY(ID)
);
/
