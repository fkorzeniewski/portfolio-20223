--Klucze obce pozwalaja nam dodać do tabeli odniesienia do innej tabli. Jest to bardzo przydatna opcja by np. powstrzymać usunięcie danych powiązanych
-- z innymi tabelami
CREATE TABLE ChinchillasTable
(
    chin_ID INTEGER GENERATED AS IDENTITY NOT NULL
    , chin_Name VARCHAR2(20) DEFAULT 'Chrupek' NOT NULL
    , chin_Age INTEGER NOT NULL
        , CONSTRAINT ChinchillasTable_PK
        PRIMARY KEY(chin_ID)
        
);
/
SELECT * FROM chinchillastable;
/
INSERT INTO ChinchillasTable(chin_Age) VALUES
    (1);
/
INSERT INTO ChinchillasTable(chin_Name,chin_Age) VALUES
    ('Buddy',1);
/
INSERT INTO ChinchillasTable(chin_Name,chin_Age) VALUES
  ('Snowflake', 2);
/
--Tworzymy tabele z właścicielami szynszyl i dzięki konstrukcji FOREIGN KEY i REFERENCES dodajemy odniesienie do tabeli szynszyl.
CREATE TABLE ChinchillasOwners
(
    own_ID INTEGER GENERATED AS IDENTITY NOT NULL
    , own_Name VARCHAR2(50) NOT NULL
    , own_Surname VARCHAR2(50) NOT NULL
    , own_ChinId INTEGER NOT NULL
        , CONSTRAINT ChinchillasOwners_PK
        PRIMARY KEY(own_ID)
        , CONSTRAINT FK_ChinchillasTable
        FOREIGN KEY (own_ChinId) REFERENCES ChinchillasTable(chin_ID)
);
/
INSERT INTO ChinchillasOwners(own_Name,own_Surname,own_ChinId)
    VALUES('Franciszek','Korzeniewski',1);
INSERT INTO ChinchillasOwners(own_Name,own_Surname,own_ChinId)
    VALUES('Maciej','Matysiak',2);
INSERT INTO ChinchillasOwners(own_Name,own_Surname,own_ChinId)
    VALUES('Barbara','Korzeniewska',3);
/
SELECT *
FROM
    chinchillastable chinTbl
    LEFT JOIN chinchillasowners own on
        chintbl.chin_id = own.own_chinid
/
--Teraz nie będziemy mogli usunąć pól z tabeli ChinchillasOwners bo są one powiązane z inną tabelą. Możemy to zmienić w defunicji tabeli.
-- Poniżej ustawiłem by przy usuwaniu rekordów usunąć też odwołania w innych tabelach
CREATE TABLE ChinchillasOwners
(
    own_ID INTEGER GENERATED AS IDENTITY NOT NULL
    , own_Name VARCHAR2(50) NOT NULL
    , own_Surname VARCHAR2(50) NOT NULL
    , own_ChinId INTEGER NOT NULL
        , CONSTRAINT ChinchillasOwners_PK
        PRIMARY KEY(own_ID)
        , CONSTRAINT FK_ChinchillasTable
        FOREIGN KEY (own_ChinId) REFERENCES ChinchillasTable(chin_ID) ON DELETE CASCADE
);
/
--tutaj natomiast przy usunięciu nastąpi ustawienie wartości null
CREATE TABLE ChinchillasOwners
(
    own_ID INTEGER GENERATED AS IDENTITY NOT NULL
    , own_Name VARCHAR2(50) NOT NULL
    , own_Surname VARCHAR2(50) NOT NULL
    , own_ChinId INTEGER NOT NULL
        , CONSTRAINT ChinchillasOwners_PK
        PRIMARY KEY(own_ID)
        , CONSTRAINT FK_ChinchillasTable
        FOREIGN KEY (own_ChinId) REFERENCES ChinchillasTable(chin_ID) ON DELETE SET NULL
);
/










