SELECT *
FROM
    tabela_kursowa
/
INSERT INTO tabela_kursowa(id,kurs_nazwa) VALUES(1,'wpis nr.1')
/
INSERT INTO tabela_kursowa VALUES(2,'wpis nr.2')
/
INSERT INTO tabela_kursowa(id,kurs_nazwa) VALUES(3,null) -- błąd kurs_nazwa nie mo�e by� nullem
/
INSERT INTO tabela_kursowa(id) VALUES(3) --tutaj zostanie dodana wartość defaultowa dla tego pola ('xxx')
/
INSERT INTO tabela_kursowa VALUES(4,'qwerty')
/
UPDATE tabela_kursowa SET kurs_nazwa = 'ytrewq' WHERE id = 4 --operacja update pozwala na modyfikacje rekordów
/
DELETE FROM tabela_kursowa WHERE id = 1;--DELETE natomiast pozwala usuwać rekordy
/