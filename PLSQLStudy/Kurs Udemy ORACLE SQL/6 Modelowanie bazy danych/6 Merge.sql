/*
    Merge daje nam możliwość wykonania kilku poleceń dml w jednej operacji (Co wspiera małą ilość kodu jego czytelność oraz 
    dzięki temu że dzieje się to w jednej transakcji jego wydajność)
*/


--stworzyłem sobie tabelę bonuses i dodałem do niego przykładowe danę
DROP TABLE Bonuses;
/
CREATE TABLE Bonuses
(
    bon_employee_id NUMBER
    , bon_department_id int
    , bon_bonus NUMBER DEFAULT 100
    , salary NUMERIC
);
/
INSERT INTO Bonuses(bon_employee_id,bon_department_id,salary)
(
    SELECT 
        e.employee_id
        , e.department_id
        , e.salary
    FROM
        employees e
    WHERE
        department_id in (10,20,30)
);
/
SELECT *
FROM
    Bonuses;
/
--Merge przypomina trochę operację from z podzapytaniem najpierw piszę sobie podzapytanie a potem łącze tabelę z podzapytania z tabęlą główną merge'a
MERGE INTO Bonuses b
    USING
    (
        SELECT 
            e.employee_id
            , e.department_id
            , e.salary
        FROM
            employees e
        WHERE
            department_id in (20,40)
    ) t2 
    ON (b.bon_employee_id = t2.employee_id)
--Kiedy już je połączyłem mogę ustawić operacje w momenkie gdy wiersze tabel się zgadzają bądź nie
WHEN
    MATCHED THEN 
        UPDATE SET b.bon_bonus = b.bon_bonus * 1.1
    WHEN NOT MATCHED THEN 
        INSERT(b.bon_employee_id,b.bon_department_id,b.salary) 
            VALUES(t2.employee_id,t2.department_id,t2.salary);
/



























