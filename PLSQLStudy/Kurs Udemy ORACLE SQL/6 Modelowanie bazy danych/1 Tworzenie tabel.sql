--Tworzenie tabel (jeszcze bez sekwencji która będzie w puźniejszej lekcji)
CREATE TABLE TABELA_KURSOWA
(
    ID INTEGER NOT NULL -- not null oznacza że przy dodawaniu rekordów nie można nie dodać wartości do pola
    , kurs_nazwa VARCHAR2(20) DEFAULT 'xxx' NOT NULL -- DEFAULT sprawi że jeżeli damy nulla w insercie to wstawi wartość z apostrofu
    , CONSTRAINT tabela_kursowa_pk 
        PRIMARY KEY(ID)
    --CONSTRAINT (ograniczenia) pozwalają nam ustawiać takie rzeczy jak klucze główne, obce itp.
);
/