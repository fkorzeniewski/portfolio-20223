--Na tej lekcji dowiedziałem się jak tworzyć tabele oraz że już w tym momencie mogę w znacznym stopniu kontrolować jakie danę przyjmę.


--W tabeli poniżej ustawiłem sobie ograniczenie że cons_Nazwa może przyjmować danę aa lub bb
CREATE TABLE TABELA_CONSTRAINT
(
    ID INTEGER GENERATED AS IDENTITY NOT NULL
    , cons_Nazwa VARCHAR2(20) NOT NULL
        , CONSTRAINT TABELA_CONSTRAINT_PK
        PRIMARY KEY (ID)
        , CONSTRAINT validate_nazwa 
        CHECK (cons_Nazwa in ('aa','bb'))
);
/
INSERT INTO TABELA_CONSTRAINT(cons_Nazwa) VALUES('aa'); --Poprawnie doda danę
/
INSERT INTO TABELA_CONSTRAINT(cons_Nazwa) VALUES('qq');--Wystąpi błąd ponieważ qq nie mieście sie w tablicy ['aa','bb']
/
-- ALTER TABLE pozwala nam modyfikować tabele. Dla przykładu poniżej dodałem cons_Liczba który nie jest nullem i ma domyślną wartość 56
ALTER TABLE TABELA_CONSTRAINT ADD 
    cons_Liczba INTEGER DEFAULT 56 NOT NULL;
/
SELECT * 
FROM
    TABELA_CONSTRAINT
/
-- Alter table pozwala nam również dodać tabelę i od razu nadać jej ograniczenia
ALTER TABLE TABELA_CONSTRAINT ADD
    cons_Age INTEGER CONSTRAINT cons_check_age CHECK(cons_Age between 0 and 150);
/
--Update da nam możliwość zmiany wartości tabeli jest to potęcjalnie bardzo niebezpieczne narzędzie ponieważ możemy nim bardzo łatwo 
--naruszyć integralność bazy danych. Zaleca się używanie go w transakcji.
UPDATE 
    TABELA_CONSTRAINT
SET 
    cons_Age = 0
WHERE
    cons_Age is null;
/