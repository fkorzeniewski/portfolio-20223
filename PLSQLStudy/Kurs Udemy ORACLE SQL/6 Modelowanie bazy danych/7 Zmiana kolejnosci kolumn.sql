/*
    W tej lekcji nauczyłem się metody by przestawić kolejność kolumn w tabeli. Jest to przydatnę kiedy chcemy do tabeli dodać nową kolumnę. Ale 
    Ma ona np. znajdować się w środku tabeli.
*/

--tworzę tabęlę testową
CREATE TABLE 
t (
    a INT,
    b INT,
    d INT,
    e INT
);
/
select * from t;
/
--dodaję nową kolumnę
ALTER TABLE t ADD (c INT);
/
--teraz korzystają z INVISIBLE WYŁĄCZAM kolumny [d,e] (zachowując danę). Ta operacja sprawiła że kolumna c wskoczyła nam do środka tabeli.
--następnie dzięki VISIBLE ponownie włączam obie te kolumny co sprawia że wskakują one już za kolumne c
ALTER TABLE t MODIFY (d INVISIBLE, e INVISIBLE);
ALTER TABLE t MODIFY (d VISIBLE, e VISIBLE);
/
--DESCRIBE wyświetliło nam dokładny opis tabeli (włącznie z interesującą nas kolejnością)
DESCRIBE t;
/