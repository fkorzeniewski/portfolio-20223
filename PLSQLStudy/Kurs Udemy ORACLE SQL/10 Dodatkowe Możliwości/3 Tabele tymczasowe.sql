/*
    Tabele tymczasowe przydają nam się np. podczas pisania skryptów PLSQL oraz gdy obawiamy się usunąć ważne dane z zwykłej tabeli.
    Są to table które nie zostaną zapisanę w bazie danych.
*/


-- Na początku stworze tabelę prywatną która usunie się po użyciu 'commit'
CREATE PRIVATE TEMPORARY TABLE 
    ORA$PTT_TEST_TABLE(ID INT) ON COMMIT DROP DEFINITION
/
-- kolejna tabele usunie się dopiero po zamknięciu sesji
CREATE PRIVATE TEMPORARY TABLE 
    ORA$PTT_TEST_TABLE(ID INT) ON COMMIT PRESERVE DEFINITION
/
SELECT 
    *
FROM
    ORA$PTT_TEST_TABLE
/
INSERT ALL
    INTO ORA$PTT_TEST_TABLE(ID) VALUES (1)
    INTO ORA$PTT_TEST_TABLE(ID) VALUES (11)
    INTO ORA$PTT_TEST_TABLE(ID) VALUES (12)
    INTO ORA$PTT_TEST_TABLE(ID) VALUES (13)
    INTO ORA$PTT_TEST_TABLE(ID) VALUES (14)
SELECT 
    1
FROM 
    DUAL
/
COMMIT
/
-- tutaj przykładowy kod plsql, który doda 100 rekordów do tabeli tymczasowej
BEGIN
    FOR I IN 1..100 
    LOOP
        INSERT INTO ORA$PTT_TEST_TABLE(ID) VALUES
        (
            TRUNC(DBMS_RANDOM.VALUE(1,101))
        );
    END LOOP;
END;
/
--Tabele globalne różnią się tym że będą widoczne dla wszystkich użytkowników (a nie dla twórcy tylko jak w przypadku prywatnej tabeli)
--Dodatkowo musimy sami świadomie ją usunąć z bazy danych
CREATE GLOBAL TEMPORARY TABLE PUBLIC_TBL(ID INT);
/
SELECT * FROM PUBLIC_TBL
/
COMMIT
/
--tutaj stworzyłem tabele tymczasową z zapytania, pobrałem pierwszysch 25 pracowników do tabeli tymczasowej
CREATE PRIVATE TEMPORARY TABLE ORA$PTT_TEST2 
    ON COMMIT DROP DEFINITION 
    AS
    (
        SELECT *
        FROM
            EMPLOYEES E
        WHERE
            ROWNUM <= 25
    )
/
SELECT * 
FROM
    ORA$PTT_TEST2
/


/*
    Temporary tables are useful, for example, when writing PL/SQL scripts and when we are concerned about deleting important data from a regular table.
    These are tables that will not be permanently stored in the database.
*/

-- First, I will create a private table that will be deleted after a 'commit'
CREATE PRIVATE TEMPORARY TABLE 
    ORA$PTT_TEST_TABLE(ID INT) ON COMMIT DROP DEFINITION
/
-- The next table will be deleted only after the session ends
CREATE PRIVATE TEMPORARY TABLE 
    ORA$PTT_TEST_TABLE(ID INT) ON COMMIT PRESERVE DEFINITION
/
SELECT 
    *
FROM
    ORA$PTT_TEST_TABLE
/
INSERT ALL
    INTO ORA$PTT_TEST_TABLE(ID) VALUES (1)
    INTO ORA$PTT_TEST_TABLE(ID) VALUES (11)
    INTO ORA$PTT_TEST_TABLE(ID) VALUES (12)
    INTO ORA$PTT_TEST_TABLE(ID) VALUES (13)
    INTO ORA$PTT_TEST_TABLE(ID) VALUES (14)
SELECT 
    1
FROM 
    DUAL
/
COMMIT
/
-- Here is an example of PL/SQL code that will add 100 records to the temporary table
BEGIN
    FOR I IN 1..100 
    LOOP
        INSERT INTO ORA$PTT_TEST_TABLE(ID) VALUES
        (
            TRUNC(DBMS_RANDOM.VALUE(1,101))
        );
    END LOOP;
END;
/
-- Global tables differ in that they will be visible to all users (unlike private tables which are only visible to the creator)
-- Additionally, we must explicitly delete them from the database
CREATE GLOBAL TEMPORARY TABLE PUBLIC_TBL(ID INT);
/
SELECT * FROM PUBLIC_TBL
/
COMMIT
/
-- Here I created a temporary table from a query, fetching the first 25 employees into the temporary table
CREATE PRIVATE TEMPORARY TABLE ORA$PTT_TEST2 
    ON COMMIT DROP DEFINITION 
    AS
    (
        SELECT *
        FROM
            EMPLOYEES E
        WHERE
            ROWNUM <= 25
    )
/
SELECT * 
FROM
    ORA$PTT_TEST2
/
