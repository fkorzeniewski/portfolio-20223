/*
    Jeżeli jest taka potrzeba to możemy też odwrócić działanie pivot komendą unpivot.
    Jest to niekiedy ciężkie zadanie
*/


--tworzymy widok z pivotem
CREATE VIEW V_PIVOT AS
WITH QUERY AS
(
    SELECT
        E.DEPARTMENT_ID
        , E.JOB_ID
    FROM
        employees E
    WHERE
        JOB_ID IN
        (
            'FI_ACCOUNT'
            , 'PU_CLERK'
            , 'IT_PROG'
            , 'MK_MAN'
            , 'MK_REP'
        )
)
SELECT 
    *
FROM
    QUERY
PIVOT
(
    COUNT(*) FOR JOB_ID IN
    (
        'FI_ACCOUNT'
        , 'PU_CLERK'
        , 'IT_PROG'
        , 'MK_MAN'
        , 'MK_REP'
    )
)
/
--wyciągamy wszystkie potrzebne nam kolumny z widoku 
WITH SUBQUERY AS
(
    SELECT 
        DEPARTMENT_ID
        , "'FI_ACCOUNT'" 
        , "'PU_CLERK'"
        , "'IT_PROG'"
        , "'MK_MAN'"
        , "'MK_REP'"
    FROM 
        v_pivot
)
SELECT *
FROM
    SUBQUERY
    --stosujemy komendę unpivot
UNPIVOT
(
    (RESULT) FOR JOB_ID IN
    (
        "'FI_ACCOUNT'" 
        , "'PU_CLERK'"
        , "'IT_PROG'"
        , "'MK_MAN'"
        , "'MK_REP'"
    )
)
/


/*
    If necessary, we can also reverse the action of a pivot command with the unpivot command.
    This can sometimes be a difficult task.
*/

-- Creating a view with pivot
CREATE VIEW V_PIVOT AS
WITH QUERY AS
(
    SELECT
        E.DEPARTMENT_ID
        , E.JOB_ID
    FROM
        employees E
    WHERE
        JOB_ID IN
        (
            'FI_ACCOUNT'
            , 'PU_CLERK'
            , 'IT_PROG'
            , 'MK_MAN'
            , 'MK_REP'
        )
)
SELECT 
    *
FROM
    QUERY
PIVOT
(
    COUNT(*) FOR JOB_ID IN
    (
        'FI_ACCOUNT'
        , 'PU_CLERK'
        , 'IT_PROG'
        , 'MK_MAN'
        , 'MK_REP'
    )
)
/
-- Extracting all necessary columns from the view
WITH SUBQUERY AS
(
    SELECT 
        DEPARTMENT_ID
        , "'FI_ACCOUNT'" 
        , "'PU_CLERK'"
        , "'IT_PROG'"
        , "'MK_MAN'"
        , "'MK_REP'"
    FROM 
        V_PIVOT
)
SELECT *
FROM
    SUBQUERY
    -- Applying the unpivot command
UNPIVOT
(
    (RESULT) FOR JOB_ID IN
    (
        "'FI_ACCOUNT'" 
        , "'PU_CLERK'"
        , "'IT_PROG'"
        , "'MK_MAN'"
        , "'MK_REP'"
    )
)
/
