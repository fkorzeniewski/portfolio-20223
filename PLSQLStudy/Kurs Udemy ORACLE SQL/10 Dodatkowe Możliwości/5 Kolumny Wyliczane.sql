/*
    Jeżeli w naszych tabelach pojawią się jakieś obliczenia np. płaca podstawowa + dodatek = płaca całościowa. To dobrą plaktyką jest 
    zastosowanie w tym przypadku kolumny wyliczanej. Baza danych sama wtedy oblicza trzecia kolumnę i co ważniejsze dba o jej integralność z pozostałymi danymi.
    Zmniejsza to liczbę updatów na bazie danych.
*/


CREATE TABLE FK_CALCULATED_COLUMNS_STUDY
(
    VALUE_A INT
    , VALUE_B INT
    --tworzymy normalnie tabelę jednak przy trzeciej wartości dodajemy AS i wyliczenia trzeciej wartości
    , VALUE_C INT AS
    (
        VALUE_A + VALUE_B
    )
)
/
-- teraz jeżeli wszystko zrobiliśmy poprawnie to VALUE_C będzie równy 15
INSERT INTO fk_calculated_columns_study(VALUE_A,VALUE_B)
    VALUES (7,8)
/
SELECT *
FROM
    FK_CALCULATED_COLUMNS_STUDY
/
--tutaj po updacie VALUE_C będzie równy 78+8
UPDATE 
    fk_calculated_columns_study 
SET
    VALUE_A = 78
/
DROP TABLE FK_CALCULATED_COLUMNS_STUDY
/

/*
    If there are any calculations in our tables, such as base salary + bonus = total salary, it is a good practice
    to use a calculated column. The database then calculates the third column automatically and, importantly, ensures its integrity with the other data.
    This reduces the number of updates on the database.
*/

CREATE TABLE FK_CALCULATED_COLUMNS_STUDY
(
    VALUE_A INT
    , VALUE_B INT
    -- We create the table normally, but for the third value, we add AS and the calculation for the third value
    , VALUE_C INT AS
    (
        VALUE_A + VALUE_B
    )
)
/
-- Now, if everything is done correctly, VALUE_C will be equal to 15
INSERT INTO fk_calculated_columns_study(VALUE_A, VALUE_B)
    VALUES (7, 8)
/
SELECT *
FROM
    FK_CALCULATED_COLUMNS_STUDY
/
-- Here, after the update, VALUE_C will be equal to 78 + 8
UPDATE 
    fk_calculated_columns_study 
SET
    VALUE_A = 78
/
DROP TABLE FK_CALCULATED_COLUMNS_STUDY
/
