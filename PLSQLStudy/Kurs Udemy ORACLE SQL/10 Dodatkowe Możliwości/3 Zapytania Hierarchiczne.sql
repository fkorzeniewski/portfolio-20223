/*
    Zapytania hierarchiczne pozwalają nam tworzyć jak nazwa wskazuje hierarchie w tabelach.
    Przydaję się to kiedy w tabeli mamy odniesienie do innego rekordu z tej samej tabeli, tak jak w przykładzie
    niżej mamy employee id oraz manager id i możemy w ładny sposób wyciagnąć hierarhie przełożonych
*/

SELECT 
    E.employee_id
    , E.last_name
    , E.manager_id
    --LEVEL pokazuje na którym poziomie hierarchi jesteśmy
    , LEVEL
FROM
    employees E
--tutaj okreśmaly po jakim warunku zapytanie ma stworzyć hierarchię 
CONNECT BY PRIOR E.employee_id = E.manager_id
/
SELECT 
    E.employee_id
    , E.last_name
    , E.manager_id
    , LEVEL
FROM
    employees E
--poniżej dodałem ograniczenie by hierarhia zaczynała się od employee_id = 101
START WITH E.employee_id = 101
CONNECT BY PRIOR E.employee_id = E.manager_id
/
select 
    employee_id,
    last_name,
    manager_id,
    level, 
    --poniższa funkcja pokaże nam jakak wygląda ścieżka w hierarchi. Pierwszy argument 
    -- określa jakie danę mamy pokazać w ścieżce, kolejny zaś jaki będzie separator
    SYS_CONNECT_BY_PATH(last_name, '/') pather,
    -- natomiast ta funkcja pokaże nam czy ścieżka nie uległa zapętleniu
    CONNECT_BY_ISCYCLE
from 
    employees
START WITH employee_id = 100
connect by NOCYCLE prior employee_id = manager_id
/
select
    -- tutaj dodatkowo nauczyłem się że funkcję LPAD i concatenacją możemy tworzyć ciekawe wcięcia
    -- które poprawią czytelniść naszej ścieżki 
    LPAD(' ', 1*level) 
        || SYS_CONNECT_BY_PATH(last_name, '/') pather
from 
    employees
START WITH employee_id = 100
connect by prior employee_id = manager_id
ORDER BY 
    LEVEL
/


/*
    Hierarchical queries allow us to create hierarchies in tables, as the name suggests.
    This is useful when a table has references to another record within the same table. For example,
    below we have employee_id and manager_id, and we can nicely extract the hierarchy of superiors.
*/

SELECT 
    E.employee_id
    , E.last_name
    , E.manager_id
    -- LEVEL shows at which level of the hierarchy we are
    , LEVEL
FROM
    employees E
-- Here we specify the condition by which the query should create the hierarchy
CONNECT BY PRIOR E.employee_id = E.manager_id
/
SELECT 
    E.employee_id
    , E.last_name
    , E.manager_id
    , LEVEL
FROM
    employees E
-- Below, I added a condition for the hierarchy to start from employee_id = 101
START WITH E.employee_id = 101
CONNECT BY PRIOR E.employee_id = E.manager_id
/
SELECT 
    employee_id,
    last_name,
    manager_id,
    level, 
    -- The function below will show us what the path in the hierarchy looks like. The first argument 
    -- specifies what data to show in the path, and the next one what the separator will be
    SYS_CONNECT_BY_PATH(last_name, '/') path,
    -- This function will show us if the path has a cycle
    CONNECT_BY_ISCYCLE
FROM 
    employees
START WITH employee_id = 100
CONNECT BY NOCYCLE PRIOR employee_id = manager_id
/
SELECT
    -- Additionally, I learned that using the LPAD function with concatenation, we can create interesting indents
    -- that improve the readability of our path
    LPAD(' ', 1*LEVEL) 
        || SYS_CONNECT_BY_PATH(last_name, '/') path
FROM 
    employees
START WITH employee_id = 100
CONNECT BY PRIOR employee_id = manager_id
ORDER BY 
    LEVEL
/
