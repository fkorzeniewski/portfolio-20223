/*
    External tables pozwalaja nam na wyświetlanie tabel, których rekordy znajdują się poza bazą danych np. w pliku csv.
    Może to być pomocne przy wyświetlaniu np. raportów zewnętrznych gdzie czesto dostajemy plik csv który niekoniecznie chcemy zapisywać w bazie danych.
*/


--na początek tworzymy w bazie danych obiekt DIRECTORY, który będzie wskazywał ścieżke do folderu
CREATE DIRECTORY GARDENERS AS 
    'path\to\directory';
/


--teraz tworzymy  tabele która zawiera kolumny z pliku csv
CREATE TABLE GARDENERS
(
    GAR_FIRST_NAME VARCHAR2(30)
    , GAR_LAST_NAME VARCHAR2(30)
    , GAR_FAVOURITE_PLANT VARCHAR2(30)
)
--dodając poniższą strukturę sprawiamy że ta tabela będzie pobierać rekordy z osobnego pliku
ORGANIZATION EXTERNAL
(
    TYPE 
        ORACLE_LOADER
    DEFAULT DIRECTORY 
        GARDENERS
    ACCESS PARAMETERS
        (FIELDS TERMINATED BY ',')
    LOCATION
        ('gardenersTable.csv')
)
/
drop table GARDENERS
/
SELECT *
FROM
    GARDENERS
/


/*
    External tables allow us to display tables whose records are located outside the database, such as in a CSV file.
    This can be helpful for displaying external reports where we often receive a CSV file that we do not necessarily want to store in the database.
*/

-- First, we create a DIRECTORY object in the database that will point to the folder path
CREATE DIRECTORY GARDENERS AS 
    'path\to\directory';
/

-- Now we create a table that contains the columns from the CSV file
CREATE TABLE GARDENERS
(
    GAR_FIRST_NAME VARCHAR2(30)
    , GAR_LAST_NAME VARCHAR2(30)
    , GAR_FAVOURITE_PLANT VARCHAR2(30)
)
-- By adding the following structure, we ensure that this table will fetch records from a separate file
ORGANIZATION EXTERNAL
(
    TYPE 
        ORACLE_LOADER
    DEFAULT DIRECTORY 
        GARDENERS
    ACCESS PARAMETERS
        (FIELDS TERMINATED BY ',')
    LOCATION
        ('gardenersTable.csv')
)
/
DROP TABLE GARDENERS
/
SELECT *
FROM
    GARDENERS
/
