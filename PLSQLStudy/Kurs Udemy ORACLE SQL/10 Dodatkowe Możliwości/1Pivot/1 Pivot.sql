WITH QUERY AS
(
    SELECT
        E.DEPARTMENT_ID
        , E.JOB_ID
    FROM
        employees E
    WHERE
        JOB_ID IN
        (
            'FI_ACCOUNT'
            , 'PU_CLERK'
            , 'IT_PROG'
            , 'MK_MAN'
            , 'MK_REP'
        )
)
SELECT 
    *
FROM
    QUERY
PIVOT
(
    COUNT(*) FOR JOB_ID IN
    (
        'FI_ACCOUNT'
        , 'PU_CLERK'
        , 'IT_PROG'
        , 'MK_MAN'
        , 'MK_REP'
    )
)
/
SELECT 
    E.DEPARTMENT_ID
    , E.JOB_ID
    , COUNT(*)
FROM
    employees E
    WHERE
        JOB_ID IN
        (
            'FI_ACCOUNT'
            , 'PU_CLERK'
            , 'IT_PROG'
            , 'MK_MAN'
            , 'MK_REP'
        )
GROUP BY
    E.DEPARTMENT_ID
    , E.JOB_ID
ORDER BY
    E.department_id
/