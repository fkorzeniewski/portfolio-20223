# Pivot

```sql
WITH QUERY AS
(
    SELECT
        E.DEPARTMENT_ID
        , E.JOB_ID
    FROM
        employees E
    WHERE
        JOB_ID IN
        (
            'FI_ACCOUNT'
            , 'PU_CLERK'
            , 'IT_PROG'
            , 'MK_MAN'
            , 'MK_REP'
        )
)
SELECT 
    *
FROM
    QUERY
PIVOT
(
    COUNT(*) FOR JOB_ID IN
    (
        'FI_ACCOUNT'
        , 'PU_CLERK'
        , 'IT_PROG'
        , 'MK_MAN'
        , 'MK_REP'
    )
)
/
```

<img src='1 Pivot.png'>

```sql
SELECT 
    E.DEPARTMENT_ID
    , E.JOB_ID
    , COUNT(*)
FROM
    employees E
    WHERE
        JOB_ID IN
        (
            'FI_ACCOUNT'
            , 'PU_CLERK'
            , 'IT_PROG'
            , 'MK_MAN'
            , 'MK_REP'
        )
GROUP BY
    E.DEPARTMENT_ID
    , E.JOB_ID
ORDER BY
    E.department_id
/
```

<img src="1 Pivot1.png">

## Pivot pozwala nam zaprezentować grupowanie w alternatywny sposób. Możemy dzięki niemu rozciągnąć widok na wiele kolumn który graficznie może być bardziej czytelny. Nie jest to najbardziej optymalne. W tym pliku najpier zaprezentowałem jak wywołać prosty pivot a potem jak różni się on względem standardowego grupowania.


## Pivot allows us to present grouping in an alternative way. It enables us to spread the view across multiple columns, which can be more visually clear. It is not the most optimal method. In this file, I first demonstrated how to invoke a simple pivot and then how it differs from standard grouping.