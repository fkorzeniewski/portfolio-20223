/*
    Tutaj jeszcze kilka przykładów zastosowania funkcji okienkowych do tworzenia raportów 
*/


--raport który zwróci nam sumę płac aktualnego , następnego i poprzedniego pracownika 
SELECT 
    E.last_name
    , E.hire_date
    , E.salary
    , SUM(E.salary) OVER
    (
        ORDER BY E.hire_date
        ROWS BETWEEN
            1 PRECEDING
            AND
            1 FOLLOWING
    ) SUMA
FROM
    employees E
/

--raport który zwróci nam sumę płac pracowników zatrudnionych 3 miesiące do przodu
SELECT 
    E.last_name
    , E.hire_date
    , E.salary
    , SUM(E.salary) OVER
    (
        ORDER BY E.hire_date
        RANGE INTERVAL
            '3' MONTH PRECEDING
            --AND
            --'1' MONTH FOLLOWING
    ) SUMA
FROM
    employees E
/


--raport sumy płac pracowników z 3 miesięcy wprzód i 3 wstecz
SELECT 
    E.last_name
    , E.hire_date
    , E.salary
    , SUM(E.salary) OVER
    (
        ORDER BY E.hire_date
        RANGE BETWEEN 
            INTERVAL '3' MONTH PRECEDING
            AND
            INTERVAL '3' MONTH FOLLOWING
    ) SUMA
FROM
    employees E
/