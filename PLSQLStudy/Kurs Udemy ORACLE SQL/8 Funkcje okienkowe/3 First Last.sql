/*
    funkcje first i last pozwalają nam szukać w alternatywny sposób pracować z maksymalnymi i minimalymi DELETE
    wartościami w tabeli. Sa uważane za bardziej przejstę i czesto zawierają mniej kodu niż podzapytania.
*/
WITH SUBQUERY AS
(
    SELECT
        sub_emp.department_id
        , min(sub_emp.salary)
    FROM
        employees SUB_EMP
    GROUP BY
        sub_emp.department_id
)
SELECT 
    emp.last_name
    , emp.department_id
    , emp.salary
FROM
    employees EMP
WHERE
    (EMP.department_id,EMP.SALARY) IN
    (
        SELECT *
        FROM
            SUBQUERY
    )
ORDER BY EMP.department_id
/


--szukanie maksymalnej wartości z funkcją keep LAST
SELECT 
    E.department_id
    , MAX(E.salary)
    , MAX(E.last_name) KEEP (DENSE_RANK LAST ORDER BY E.salary)
FROM
    employees E
GROUP BY
    E.department_id
/


--szukanie minimalnej wartości z funkcją keep FIRST
SELECT 
    E.department_id
    , MIN(E.salary)
    , MIN(E.last_name) KEEP (DENSE_RANK FIRST ORDER BY E.salary)
FROM
    employees E
GROUP BY
    E.department_id
/











