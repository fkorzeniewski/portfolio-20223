/*
    LAG I LEAD pozwalają nam wyświetlić rekord poprzedzający (lag) i następujący (lead) w zależności od aktualnego rekordu.


    LAG(informacje którechcemy wyświetlić,na ile rekordów wstecz patrzymy,co wyświetlić jeżeli rekord nie istnieje)    
    LEAD(informacje którechcemy wyświetlić,na ile rekordów do przodu patrzymy,co wyświetlić jeżeli rekord nie istnieje)    


    OVER(ORDER BY po jakim polu patrzymy wstecz albo wprzód)
*/


SELECT 
    E.first_name
    , E.last_name
    , E.hire_date
    , E.department_id
    , LAG(E.first_name || '' || E.last_name, 1,'---') OVER (ORDER BY E.hire_date) PREVIOUS_EMP
    , LEAD(E.first_name || '' || E.last_name, 1,'---') OVER (ORDER BY E.hire_date) LED_EMP
FROM
    employees E
ORDER BY
    E.department_id
/
/*
    Oczywiście jako są to funkcje okienkowe to możemy użyć partycjonowania danych i stworzyć 
    bardziej zaawansowane widoki
*/
SELECT 
    E.first_name
    , E.last_name
    , E.hire_date
    , E.department_id
    , LAG(E.first_name || '' || E.last_name, 1,'---') OVER (PARTITION BY E.department_id ORDER BY E.hire_date) PREVIOUS_EMP
    , LEAD(E.first_name || '' || E.last_name, 1,'---') OVER (PARTITION BY E.department_id ORDER BY E.hire_date) LED_EMP
FROM
    employees E
ORDER BY
    E.department_id
/










