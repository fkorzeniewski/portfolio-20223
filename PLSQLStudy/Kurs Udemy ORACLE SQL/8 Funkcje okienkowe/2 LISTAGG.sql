/*
    LISTAGG pozwala nam wyświetlić danę w troszkę nietypowy sposób. Funkcja ta jest funkcją grupującą wiec
    oczywiście wymaga jakiegoś grupowania. Następnie wyświetla ona wszystkie dane w jednym wierszu.
*/


/*
    Dla przykładu to zapytanie wyświetli nam wszystkie nazwiska pracowników w danym departamencie.
*/
SELECT 
    E.department_id
    , LISTAGG(E.last_name,';') WITHIN GROUP (ORDER BY E.last_name) EMPS
FROM
    employees E
GROUP BY
    E.department_id
/