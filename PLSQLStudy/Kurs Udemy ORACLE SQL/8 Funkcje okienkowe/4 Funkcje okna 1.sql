/*
    Poniższy przykład ma w prosty sposób pokazać nam możliwości wykorzystania okien do zaawansowanych raportów.
    Chcemy tutaj wyświetlić datę zatrudnienia pracownika, pensję oraz informacje ile dotychczasowo poniędzy przeznaczyliśmy na wypłaty.
    pracowników. Oba zapytania wyświetlą to samo jednak w pierwszym przypadku użyliśmy funkcji between
*/


SELECT 
    E.hire_date
    , E.salary
    , SUM(E.salary) OVER
    (
        ORDER BY E.hire_date
            ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
    ) GROW
FROM
    employees E
ORDER BY
    E.hire_date
/


SELECT 
    E.hire_date
    , E.salary
    , SUM(E.salary) OVER
    (
        ORDER BY E.hire_date
            ROWS UNBOUNDED PRECEDING
    ) GROW
FROM
    employees E
ORDER BY
    E.hire_date
/