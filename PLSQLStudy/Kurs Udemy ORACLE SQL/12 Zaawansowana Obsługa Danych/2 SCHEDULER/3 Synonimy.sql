-- Synonimy pomagają nam uporządkować bazę danych i dostępy do różnych tabel z poziomów różnych użytkowników.
-- Są dwa typy synonimów:
--     - Publiczny który odnosi się wprost do obiektu np.
--     - Prywatny, który odnosi się do uzytkowników którzy dostali uprawnienia dla synonimu


create table t2 (id int)
/
insert into t2 values (1)
/
create public synonym syn_t2 for t2
/
grant select on syn_t2 to user2
/

create synonym syn2_t2 for t2
/
grant select on syn2_t2 to user2
