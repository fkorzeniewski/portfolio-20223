-- linki bazodanowe pozwalają nam się łączyć do innych baz danych i traktować je 
-- jak lokalne tabele, umożliwiając tym samym wykonywanie zapytań oraz operacji na danych
--  w sposób zdalny, bez konieczności fizycznego przenoszenia danych pomiędzy serwerami. 
--  Dzięki temu możemy integracji danych z różnych źródeł,
-- co pozwala na bardziej kompleksową analizę i raportowanie, a także na zwiększenie elastyczności w zarządzaniu danymi.


create database link testlink
connect to zskstudentdane identified by ""
USING
'(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=185.167.98.41)(PORT=1521))(CONNECT_DATA=(SID=XE)))'
/
select * from test@testlink

/
select * from user_db_links
/