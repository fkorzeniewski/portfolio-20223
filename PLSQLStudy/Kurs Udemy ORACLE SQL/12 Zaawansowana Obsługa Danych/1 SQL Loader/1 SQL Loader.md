# SQL LOADER

### Stwórz tabelę do której załadujesz danę

```sql
CREATE TABLE STUDY_FK_SHOPPING
(
    SHOPPING_ID INT 
    , NAME VARCHAR2(20)
    , PRICE INT
)
/
DESCRIBE STUDY_FK_SHOPPING
/
```

### Otwórz wiersz poleceń

### Sprawdź czy jest zainstalowany sql loader komendą `sqlldr`

### Jeżeli jest zainstalowany to przygotuj plik konfiguracyjny `settings.txt` (nazwa może być inna)

```
LOAD DATA
INFILE 'ścieżka do pliku z zasobami (format .txt/.csv)'
BADFILE 'ścieżka do pliku z błędami (format .BAD)'
REPLACE/TRUNCATE/APPEND (WYBIERZ JEDNO)
INTO TABLE STUDY_FK_SHOPPING
FIELDS TERMINATED BY ','
(SHOPPING_ID,NAME,PRICE)
```

### W wierszu poleceń napisz

```
sqlldr system/franek111 CONTROL='ścieżka do pliku settings' LOG='ścieżka gdzie mają się zapisać loggi'
```

### Jeżeli wszystko pujdzie dobrze to dostaniemy takie informacje w konsoli

<img src="1.png">

### możemy jeszcze zobaczyć nasz plik z logami, który przedstawi nam przebieg załadowania danych.

```

SQL*Loader: Release 21.0.0.0.0 - Production on Sat Jul 6 16:24:04 2024
Version 21.3.0.0.0

Copyright (c) 1982, 2021, Oracle and/or its affiliates.  All rights reserved.

Control File:   C:\Users\franek\Desktop\SQL_LOADER_STUDY\settings.txt
Data File:      C:\Users\franek\Desktop\SQL_LOADER_STUDY\shopping_data.csv
  Bad File:     C:\Users\franek\Desktop\SQL_LOADER_STUDY\SHOPPING.BAD
  Discard File:  none specified
 
 (Allow all discards)

Number to load: ALL
Number to skip: 0
Errors allowed: 50
Bind array:     250 rows, maximum of 1048576 bytes
Continuation:    none specified
Path used:      Conventional

Table STUDY_FK_SHOPPING, loaded from every logical record.
Insert option in effect for this table: APPEND

   Column Name                  Position   Len  Term Encl Datatype
------------------------------ ---------- ----- ---- ---- ---------------------
SHOPPING_ID                         FIRST     *   ,       CHARACTER            
NAME                                 NEXT     *   ,       CHARACTER            
PRICE                                NEXT     *   ,       CHARACTER            

Record 1: Rejected - Error on table STUDY_FK_SHOPPING, column SHOPPING_ID.
ORA-01722: invalid number


Table STUDY_FK_SHOPPING:
  20 Rows successfully loaded.
  1 Row not loaded due to data errors.
  0 Rows not loaded because all WHEN clauses were failed.
  0 Rows not loaded because all fields were null.


Space allocated for bind array:                 193500 bytes(250 rows)
Read   buffer bytes: 1048576

Total logical records skipped:          0
Total logical records read:            21
Total logical records rejected:         1
Total logical records discarded:        0

Run began on Sat Jul 06 16:24:04 2024
Run ended on Sat Jul 06 16:24:05 2024

Elapsed time was:     00:00:01.58
CPU time was:         00:00:00.08

```

### Możemy z niego przeczytać że jeden wiersz sie nie zaczytał i rzeczywiście kiedy spojżymy do pliku .BAD

```
SHOPPING_ID,NAME,PRICE
```

### Dowiemy się że pierwszy wiersz nie został zaczytany (jest to wiersz z nagłówkiem który nie pasuje do specyfikacji kolumn)

### Naprawdę warto korzystać z tego narzędzia

```

SQL*Loader: Release 21.0.0.0.0 - Production on Sat Jul 6 16:32:22 2024
Version 21.3.0.0.0

Copyright (c) 1982, 2021, Oracle and/or its affiliates.  All rights reserved.

Control File:   C:\Users\franek\Desktop\SQL_LOADER_STUDY\settings.txt
Data File:      C:\Users\franek\Desktop\SQL_LOADER_STUDY\shopping_data.csv
  Bad File:     C:\Users\franek\Desktop\SQL_LOADER_STUDY\SHOPPING.BAD
  Discard File:  none specified
 
 (Allow all discards)

Number to load: ALL
Number to skip: 0
Errors allowed: 50
Bind array:     250 rows, maximum of 1048576 bytes
Continuation:    none specified
Path used:      Conventional

Table STUDY_FK_SHOPPING, loaded from every logical record.
Insert option in effect for this table: APPEND

   Column Name                  Position   Len  Term Encl Datatype
------------------------------ ---------- ----- ---- ---- ---------------------
SHOPPING_ID                         FIRST     *   ,       CHARACTER            
NAME                                 NEXT     *   ,       CHARACTER            
PRICE                                NEXT     *   ,       CHARACTER            


Table STUDY_FK_SHOPPING:
  306433 Rows successfully loaded.
  0 Rows not loaded due to data errors.
  0 Rows not loaded because all WHEN clauses were failed.
  0 Rows not loaded because all fields were null.


Space allocated for bind array:                 193500 bytes(250 rows)
Read   buffer bytes: 1048576

Total logical records skipped:          0
Total logical records read:        306433
Total logical records rejected:         0
Total logical records discarded:        0

Run began on Sat Jul 06 16:32:22 2024
Run ended on Sat Jul 06 16:32:24 2024

Elapsed time was:     00:00:01.90
CPU time was:         00:00:00.19
```

### W ciągu niecałych dwóch sekund udało mi się dodać ponad 300 tyś rekordów do tabeli jest to szalenie wydajne