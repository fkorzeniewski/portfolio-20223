-- Podzapytanie skorelowanie używa w swoich warunkach kolumn z zapytania nadrzednego. Jest ono ciężkie dla bazy
-- danych ponieważ wykona się ono tyle razy ile wystąpi job_id z zapytania nadrzędnego
select
    *
from
    employees emp
where
    salary > (
        select
            avg(salary)
        from
            employees
        where
            job_id = emp.job_id
    )
    -- Możemy je też wykorzystać w selectie
select
    e.last_name,
    e.first_name,
    (
        select
            j.job_title
        from
            jobs j
        where
            j.job_id = e.job_id
    )
from
    employees e


-- warto wspomnieć o operatorze exists, który pomorze nam sprawdzić czy np. tak jak w przykłanie istnieje
-- departament dla którego nie istnieje pracownik. Znowu nie jest to najoptymalniejszo rozwiązanie.
select
    d.department_id,
    d.department_name
from
    departments d
where
    not exists (
        select
            1
        from
            employees e
        where
            e.department_id = d.department_id
    )


-- konkluzja na koniec zapytanie skorelorawane będzie się zawsze wykonywać tyle razy ile wystąpą dane nadrzędne