/*
    W tej lekcji dowiedziałem się więcej o optymalizacji zapytań. W pierwszym przykładzie użyliśmy podzapytania w klauzuli FROM by w WHERE móc
    skorzystać z aliasu dla obliczenia całego wynagrodzenia (full_salary). Jest to ciekawe ponieważ pod kontem optymalizacji stoi na podobnym
    poziomie jak drugie zapytanie, które nie korzysta z podzapytania. Dalej w 3 przykładzie używiśmy CTE i zapisaliśmy podzapytanie w pamięci podręcznej
    bazy danych. Dzieki temu też mogliśmy skorzystać z aliasu dla full_salary i o dziwo też optymalizacyjnie stoi to na takim samym poziomie jak
    dwa poprzednie wyrażenia
*/


SELECT 
    t1.*
FROM
(
    SELECT 
        last_name
        , salary + (salary * NVL(commission_pct,0)) full_salary
    FROM
        employees e
) t1
WHERE
    t1.full_salary > 10000
/

SELECT 
    last_name
    , salary + (salary * NVL(commission_pct,0)) full_salary
FROM
    employees e
WHERE
    salary + (salary * NVL(commission_pct,0)) >10000
/

--CTE COMMON TABLE EXPRESSION
WITH T2 AS
(
    SELECT 
        last_name
        , salary + (salary * NVL(commission_pct,0)) full_salary
    FROM
        employees
)
SELECT *
FROM
    T2
WHERE
    full_salary > 10000
/