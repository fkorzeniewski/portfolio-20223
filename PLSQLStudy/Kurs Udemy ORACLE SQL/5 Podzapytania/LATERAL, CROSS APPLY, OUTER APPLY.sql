/*
    Łączenia tabel z podzapytaniami.
*/
/

-- To zapytanie zwróci nam błąd w taki sposób nie połaczymy tabeli z podzapytaniem
SELECT
    *
FROM
    EMPLOYEES E,
    (
        SELECT
            DEPARTMENT_NAME
        FROM
            DEPARTMENTS D
        WHERE
            D.DEPARTMENT_ID = E.DEPARTMENT_ID
    )         

/


-- LATERAL pozwala nam użyć cross join'a z podzapytaniem 
SELECT
    *
FROM
    EMPLOYEES   E,
    LATERAL (
        SELECT
            DEPARTMENT_NAME
        FROM
            DEPARTMENTS D
        WHERE
            D.DEPARTMENT_ID = E.DEPARTMENT_ID
    )           

/


-- CROSS APPLY pozwala nam zastosować połączenie wewnętrzne (join)
SELECT *
FROM
    employees e
    CROSS APPLY
    (
        SELECT 
            department_name 
        from 
            departments d
        WHERE
            d.department_id = e.department_id
    )
/


-- OUTER APPLY pozwala nam połączyć zewnętrznie tabele z podzapytaniem (left join)
SELECT *
FROM
    employees e
    OUTER APPLY
    (
        SELECT 
            department_name 
        from 
            departments d
        WHERE
            d.department_id = e.department_id
    )