/*
Podzapytania pozwalają nam użyć jako warunku wyświetlenia danych innego zapytania.
Jest to potężne narzędzie którym możemy tworzyć naprawdę rozbudowane widoki danych jednak.
trzeba to robic świadomie i rozważnie by nie spowalniać niepotrzebnie bazy danych
 */


--wyświetl pracowników którzy pracują w departamentach zawierających w swojej nazwie literę 'o'
-- jest to podzapytanie jednowierszowe
SELECT
    *
FROM
    EMPLOYEES EMP
WHERE
    EMP.DEPARTMENT_ID in (
        SELECT
            dep.department_id
        FROM
            DEPARTMENTS DEP
        WHERE
            DEP.department_name like '%o%'
    );


--Wyświetl pracowników którzy  pochodzą z departamentu id >= 60 i zarabiają minimalnie 9000
SELECT
    *
FROM
    EMPLOYEES EMP
WHERE
    EMP.DEPARTMENT_ID in (
        SELECT
            dep.department_id
        FROM
            DEPARTMENTS DEP
        WHERE
            dep.department_id <= 60
    )
    AND EMP.JOB_ID IN (
        SELECT
            JBS.JOB_ID
        FROM
            JOBS JBS
        WHERE
            JBS.MIN_SALARY >= 9000
    )


    -- podzapytania wielokolumnowe to takie które odnoszą się w warunku do wielu pól, przydają się one tak jak w 
    -- poniższym przykładzie do funkcji agregujących. W tym zapytaniu wyświetliliśmy najlepiej zarabiajacych
    -- pracowników z każdego departamentu
select
    *
from
    employees
where
    (department_id, salary) in (
        select
            emp.department_id,
            max(salary)
        from
            employees emp
        group by
            emp.department_id
    )
order by
    department_id


    -- operator any pozwoli w poniższym przykładzie pozwoli nam użyć podzapytania nawet jeżeli program oczekuje z niego
    -- jednej wartości, tak samo operator all nam na to pozwoli 
select
    *
from
    employees emp
where
    salary > any (
        select
            salary
        from
            employees
        where
            department_id = 60
    )
select
    *
from
    employees emp
where
    salary > all (
        select
            salary
        from
            employees
        where
            department_id = 60
    )

    
    -- są one jednak bardzo cieżkie dla bazy danych lepiej w tym przypadku w podzapytaniu użyć funkcji agregującej min
select
    *
from
    employees emp
where
    salary > (
        select
            min(salary)
        from
            employees
        where
            department_id = 60
    )