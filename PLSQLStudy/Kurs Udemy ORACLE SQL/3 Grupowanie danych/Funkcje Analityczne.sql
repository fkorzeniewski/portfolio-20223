/*
    Funkcje analityczne pozwalają nam tworzyć zaawansowane raporty z naszych tabel. Na tej lekcji 
    nauczyłem się zaledwie kilku a już dają one potężne możliwości raportowania danych.
*/


/*
    ROLLUP pozwala nam dodać na końcu podsumowanie naszej funkcji grupującej
    tutaj mamy pokazanie etatów w poszczegónych działach z właśnie przedstawionym podsumowaniem w 
    ostatniej lini
*/
SELECT 
    NVL(TO_CHAR(ID_ZESP),'SUMMARY') TEAM
    , COUNT(*) JOB_COUNT
FROM
    PRACOWNICY
GROUP BY 
    ROLLUP(ID_ZESP)


/*
    do rollup możemy podawać wiele pozycji co znacząco wydłuży nam wynik zapytania ale doda grupowanie 
    i podsumowanie każdej grupy
*/
SELECT 
    NVL(TO_CHAR(ID_ZESP),'TEAM ID SUMMARY') TEAM
    , NVL(ETAT,'JOB SUMMARY')
    , COUNT(*) JOB_COUNT
FROM
    PRACOWNICY
GROUP BY 
    ROLLUP(ID_ZESP,ETAT)
ORDER BY 
    ID_ZESP


/*
    CUBE przy jednej kolumnie grupującej pozornie nic nie zmienia względem rollup. Jednak gdy dodamy
    kolejne kolumny to zwórci on nam podsumowanie każdej grupy I KAŻDEJ KOLUMY. W przykładnie poniżej
    dostaniemy podsumowanie dla etatów w każdym zespole a na koniec ogólne podsumowanie dla każdego
    etatu.
*/
SELECT 
    NVL(TO_CHAR(ID_ZESP),'TEAM ID SUMMARY') TEAM
    , NVL(ETAT,'JOB SUMMARY')
    , COUNT(*) JOB_COUNT
FROM
    PRACOWNICY
GROUP BY 
    cube(ID_ZESP,ETAT)
ORDER BY 
    ID_ZESP


/*
    GROUPING_ID({NAME}) daje nam możliwość sprawdzenia który wiersz odnosi się do jakiego filtrowania.
    Kiedy np. grupowanie będzie odnosić się do ETAT do w kolumnie GROUPING_ID będziemy mieli "1" 
    inaczej będzie to "0". Co ciekawe możemy w klauzuli having filtrować sobie zapytanie właśnie po
    tym polu.
*/
SELECT 
    NVL(TO_CHAR(ID_ZESP),'TEAM ID SUMMARY') TEAM
    , NVL(ETAT,'JOB SUMMARY') JOB
    , COUNT(*) JOB_COUNT
FROM
    PRACOWNICY
GROUP BY 
    cube(ID_ZESP,ETAT)
HAVING
    --GROUPING_ID(ID_ZESP) = 1
    GROUPING_ID(ETAT) = 1
ORDER BY 
    ID_ZESP


/*
    Ostatni na tej lekcji był GROUPING SETS który pozwala nam na zastosowanie kilku różnych grupowań w 
    jednym zapytaniu. Może to rodzić duplikaty różnych wierszy dlatego też zastosowałem tutak 
    GROUP_ID() który mówi nam czy dany wiersz wystąpił już w grupowaniu. Wtedy możemy w prosty sposób
    pozbyć się duplikatów w klauzuli having
*/
SELECT 
    NVL(TO_CHAR(ID_ZESP),'TEAM ID SUMMARY') TEAM
    , NVL(ETAT,'JOB SUMMARY') JOB
    , COUNT(*) JOB_COUNT
FROM
    PRACOWNICY
GROUP BY 
    GROUPING SETS((ID_ZESP),(ID_ZESP,ETAT),(ETAT,ID_ZESP))
HAVING
    GROUP_ID() = 0
ORDER BY 
    ID_ZESP
    , ETAT