/*
    Group by i having pozwala nam przedstawić danę z tabeli w uporządkowany przejszysty sposób.
    Otwiera nam to bardzo dużo możliwości prezentowania danych i sumowania ich na nasz własne potrzeby
*/


--pokazanie ile pracowników pracuje w każdym dziale
SELECT 
    ID_ZESP
    , COUNT(*) ILOSC_ETATOW_W_DZIALE
FROM
    PRACOWNICY
GROUP BY 
    ID_ZESP


--pokazanie ile pracowników I NA JAKIM ETACIE pracują w poszczegulnych działach
SELECT 
    ID_ZESP
    , ETAT
    , COUNT(*) ILOSC_ETATOW_W_DZIALE
FROM
    PRACOWNICY
GROUP BY 
    ID_ZESP
    , ETAT
ORDER BY
    ID_ZESP


/*
    Tutaj dodaliśmy przefiltrowanie wyników. Funkcje grupujące filtrujemy w klauzuli having.
    Z strony optymalizacji zapytania ważnym jest by w tej klauzuli filtrować tylko funkcje grupujące
    a resztę w klauzuli where 
*/
SELECT 
    ID_ZESP
    , ETAT
    , COUNT(*) ILOSC_ETATOW_W_DZIALE
FROM
    PRACOWNICY
GROUP BY 
    ID_ZESP
    , ETAT
HAVING
    COUNT(*) >= 2
ORDER BY
    ID_ZESP


SELECT 
    ID_ZESP
    , ETAT
    , COUNT(*) ILOSC_ETATOW_W_DZIALE
FROM
    PRACOWNICY
WHERE
    ID_ZESP >20
GROUP BY 
    ID_ZESP
    , ETAT
HAVING
    COUNT(*) >= 2
ORDER BY
    ID_ZESP