/*
    Oto podstawowe funkcje grupujące. Count zlicza tylko wartości które 
    nie są nullem to samo avg co może czasami prowadzić do różnych wyników
*/


SELECT 
    COUNT(*) --zlicza rekordy które nie są nullami
    , SUM(PLACA_POD) --sumuje place podstawową
    , ROUND(AVG(PLACA_POD),2) --oblicza średnią płacy podstawowej z zaogrągleniem do 2 miejst po przecinku
    , MAX(PLACA_POD) --zwraca maksymalną płace podstawową
    , MIN(PLACA_POD) --zwraca minimalną płace podstawową
    -- dwa poniższe wiersze pokazują nam właśnie że count nie pokazuje nulli
    -- jeden rekord zwróci 14 a drugi 6 
    , COUNT(PLACA_POD)
    , COUNT(PLACA_DOD)
    -- poniżej koleny przykład nie zliczania nulli średnie płacy będą się różnić w zależności od tego
    -- czy chcemy traktować wartość null jako 0 albo w ogóle nie brać jej pod uwagę 
    , ROUND(AVG(PLACA_DOD),2)
    , ROUND(AVG(NVL(PLACA_DOD,0)),2)
    -- tutaj ciekawe zliczanie unikalnych wierszy jest to jednak sposób mocno spowalniający zapytanie
    , COUNT(DISTINCT ETAT)
    , MIN(PLACA_DOD)
FROM
    PRACOWNICY