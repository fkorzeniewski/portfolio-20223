/*
    Oprócz łączenia kolumn możemy też "doklejać" wiersze. Zapytanie poniżej połaczy nam oba zapytania. Co ciekawe jako że oba wiersze są jednakowe
    wynikiem tego zapytania będzie jeden wiersz.
*/
SELECT 1,2
FROM
    DUAL
UNION
SELECT 1,2
FROM
    DUAL


/*
    UNION ALL sprawi że pomimo że oba wiersze są jednakowe to i tak wyświetli je wszystkie
*/
SELECT 1,2
FROM
    DUAL
UNION ALL
SELECT 1,2
FROM
    DUAL


/*
    INTERSECT zwróci nam cześć wspólną łączonych tabel. Poniższy kod nie zwróci nam rekordów ponieważ trzecia tabela różni się od pozostałych
*/
SELECT 1,2
FROM
    DUAL
INTERSECT
SELECT 1,2
FROM
    DUAL
INTERSECT
SELECT 1,3
FROM
    DUAL


/*
    MINUS odejmuje wiersze z pierwszej wartości. poniższy kod zwórci nam wyniki [1,3] dlaczego? Ponieważ od wartości [1,3] odemniemy [1,4] 
    (nie są równe więc nic się nie zdarzy). A potem od [1,3] odejmiemy [1,2] (tak samo nic się nie stanie). Jest to przydatne do sprawdzania danych po
    np. backupie, po odpaleniu programu itp.
*/
SELECT 1,3
FROM
    DUAL
MINUS
SELECT 1,4
FROM
    DUAL
MINUS
SELECT 1,2
FROM
    DUAL