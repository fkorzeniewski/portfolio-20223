/*
    REGEXP_INSTR oraz REGEXP_SUBSTR dają nam bardziej wydajmą alternatywę dla standardowych funkjci instr i substr. Warto wspomnieć że 
    jako są to wyrażenia regularne możemy użyć w nich ich specyficznej składni by uzyskać zaawansowane efekty wyszukiwania oraz przycinania DELETE
    ciągów.
*/


SELECT 
    'Najnoxxwszy tekst'
    -- ta funkcja zwróci mi pozycje drugiego x w tym ciągu
    , REGEXP_INSTR('Najnoxxwszy tekst','x',1,2) INSTR
FROM
    DUAL
/
SELECT 
    'Najnoxxwszy4 tekst'
    --tutaj szykać będziemy pierwszej cyfry w ciągu 
    , REGEXP_INSTR('Najnoxxwszy4 tekst','[[:digit:]]',1,1) INSTR
FROM
    DUAL
/
SELECT
    'TO JEST NOWY TEKS'
    --w tym przypadku chcemy przyciąć ciąg od litery J do końca ciągu
    , REGEXP_SUBSTR('TO JEST NOWY TEKS','J.{1,}') substr
FROM
    DUAL
/
SELECT
    'TO JEST NOWY TEKS'
    -- tutaj z ciągu chcemy wyciągnąć literę J następnie jakie kolwiek litery
    -- uwaga tutaj wynikiem będzie słowo 'JEST' ponieważ spacja nie list liczona jako [[:alpha:]]
    , REGEXP_SUBSTR('TO JEST NOWY TEKS','J[[:alpha:]]{1,}') substr
FROM
    DUAL
/
SELECT
    'TO JEST NOWY TEKS'
    --tutaj bliżniaczy przykład jak powyżej z tą różnicą że chcemy wyciągnąć kolejne słowo
    , REGEXP_SUBSTR('TO JEST NOWY TEKS','J[[:alpha:]]{1,} [[:alpha:]]{1,}') substr
FROM
    DUAL
/