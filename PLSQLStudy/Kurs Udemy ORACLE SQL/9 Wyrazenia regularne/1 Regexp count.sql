/*
    Wyrażenia regularne otwierają nam dużą furtkę do validacji danych i analizy ciagów znaków. W tej lekcji skupiłem się na REGEXP_COUNT
    który skupia się na zliczeniu różnych znaków z ciągów.
*/


--tutaj sprawdzamy ile liter 'a' jest w ciągu 'NAPIS'. Warto zwrócić uwagę na to że użyłem funkcje LOWER ponieważ REGEXP_COUNT jest case sensitive.
SELECT 
    REGEXP_COUNT(LOWER('NAPIS'),'a') val
FROM
    DUAL
/


-- '[[:alpha:]]' będzie szukał jakich kolwiek LITER. tutaj odpoiwiedzia będzie 5
SELECT 
    REGEXP_COUNT(LOWER('NAPIS'),'[[:alpha:]]') val
FROM
    DUAL
/


--tutaj też ponieważ dodane cyfry nie będą branę pod uwagę
SELECT 
    REGEXP_COUNT(LOWER('NA2PI3S'),'[[:alpha:]]') val
FROM
    DUAL
/


-- Sytuacja odwrotna wynikiem tego będzie 2 ponieważ mamy w tym ciągu dwie liczby. '[[:digit:]]' szuka cyfr
SELECT 
    REGEXP_COUNT(LOWER('NA2PI3S'),'[[:digit:]]') val
FROM
    DUAL
/


-- '[[:alnum:]]' Szyka i cyfr i liter ALE NIE znaków specialnych dlatego to zapytanie zwróci liczbę 7
SELECT 
    REGEXP_COUNT(LOWER('NA2PI3S#'),'[[:alnum:]]') val
FROM
    DUAL
/


-- '.' szulkać będzie każdego znaku więc odpowiedzią na to zapytanie będzie liczba 8
SELECT 
    REGEXP_COUNT(LOWER('NA2PI3S#'),'.') val
FROM
    DUAL
/