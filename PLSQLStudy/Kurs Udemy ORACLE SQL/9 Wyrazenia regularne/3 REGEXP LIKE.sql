/*
    REGEXP_LIKE to potężne narzędzie, które trzeba znać. Jest ono alternatywą dla klauzuli LIKE ale działa szybciej i daję potężne 
    możliwości walidacji ciągów znaków.
*/


SELECT 
    'TRUE'
FROM
    DUAL
WHERE
    -- to zapytanie zwróci true jeżeli w ciągu 'TEST' znajduje się litera 'e'
    -- warto powiedzieć że dodałem tutaj przeciążenie 'i' które sprawia że funkcja będzie ignorować wielkość znaków.
    REGEXP_LIKE('TEST','e','i')
/
SELECT 
    'TRUE'
FROM
    DUAL
WHERE
    -- znak '^' sprawi że będziemy sprawdzać czy ciąg zaczyna się od pewnych znaków
    REGEXP_LIKE('TEST','^T','i')
/
SELECT 
    E.last_name
FROM
    employees E
WHERE
    -- analogicznie '$' sprawdza czy ciąg kończy się na daną literę
    REGEXP_LIKE(E.last_name,'S$','i')
/
SELECT 
    E.last_name
FROM
    employees E
WHERE
    -- tutaj patrzymy czy nazwisko zaczyna się na 'k', następnie ma dwa jakiekolwiek znaki i kończy się na 'o'
    REGEXP_LIKE(E.last_name,'^K..O$','i')
/
SELECT 
    E.last_name
FROM
    employees E
WHERE
    -- {2,} tutaj podobna sytuacja ale chcemy by ten byle jaki znak wystąpił co najmniej 2 razy
    REGEXP_LIKE(E.last_name,'^K.{2,}O$','i')
/
SELECT 
    E.last_name
FROM
    employees E
WHERE
    -- [[:alpha:]] będzie sprawdzał czy znak jest literą
    REGEXP_LIKE(E.last_name,'^K[[:alpha:]]{1,}O$','i')
/
SELECT 
    E.last_name
FROM
    employees E
WHERE
    -- {2} tutaj ograniczamy żę litery muszą wystąpić konkretnie 2 razy
    REGEXP_LIKE(E.last_name,'^K[[:alpha:]]{2}G$','i')
/
SELECT 
    E.last_name
FROM
    employees E
WHERE
    -- | odpowiada równianiu logicznemu OR tutaj sprawdzamy czy nazwisko zaczyna się na K albo L
    REGEXP_LIKE(E.last_name,'^K|^L','i')
/
SELECT 
    E.last_name
FROM
    employees E
WHERE
    -- analogicznie sprawdzamy czy kończy się na A albo O
    REGEXP_LIKE(E.last_name,'A$|O$','i')
/
SELECT 
    E.last_name
FROM
    employees E
WHERE
    -- [AO] sprawdzi nam czy w ciągu znaków występu litery z tablicy w tym przypadku A albo O
    REGEXP_LIKE(E.last_name,'[AO]','i')
/
SELECT 
    E.last_name
FROM
    employees E
WHERE
    -- sytuacja podobna ale patrzymy na litery od A do C
    REGEXP_LIKE(E.last_name,'[A-C]','i')
/
SELECT
    'TRUE'
FROM
    DUAL
WHERE
    -- a tutaj praktyczna walidacja numeru PESEL chcemy by miał od 11 liter i uwaga musimy dodać znaki początku i końca by działało to poprawnie
    REGEXP_LIKE('12345678909','^[[:digit:]]{11}$')
/