/*
    Na początku dodam szybko że REGEXP_COUNT możemy przeciążać i dzięki temu tak jak w poprzedniej lekcji korzystałem z funkcji LOWER by 
    tekst zamienić na małe litery. Tak możemy też dodać przeciążenie 'i' by REGEXP_COUNT ignorował wielkość liter
*/


SELECT 
    REGEXP_COUNT('NAPIS','a',1,'i')
FROM
    DUAL
/

/*
    REGEXP_REPLACE pozwala nam w dużym stopniu zamieniać dynamicznie litery w ciągu znaków na inne. Tutaj też nauczyłem się o kilku 
    przydaynych przeciążeniach tej funkcji.
*/


SELECT
    -- ta linia w ciągu znaków wyszuka litery 'o' i zamieni je na znak '#'
    REGEXP_REPLACE('to jest dlu�szy teks o programowaniu w j�zyku SQL','o','#') a
    -- poniższe przeciążenia sprawiaże sprawdzanie zaczniemy od 4 pozycji i zamienimy tylko drugą napotkaną literę 'o'
    , REGEXP_REPLACE('to jest dlu�szy teks o programowaniu w j�zyku SQL','o','#',4,2) b
    -- tutaj zaczniemy od 4 poycji ale będziemy już zamieniać znaki do końca
    , REGEXP_REPLACE('to jest dlu�szy teks o programowaniu w j�zyku SQL','o','#',4,0) c
    -- tak jak napisałem wcześniej możemy dodać jeszcze przeciążenie 'i' by funkcja ignorowała wielkości znaków
    , REGEXP_REPLACE('to jest dlu�szy teks o programowaniu w j�zyku SQL','o','#',1,0,'i') d
FROM
    DUAL
/