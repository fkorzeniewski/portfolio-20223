-- W klauzuli model możemy zaadresować zakres komórek które będziemy modyfikować w modelu.


SELECT 
    EMPLOYEE_ID
    , SALARY
FROM
    EMPLOYEES EMP
MODEL
    DIMENSION BY(EMPLOYEE_ID)
    MEASURES(SALARY)
    RULES
    (
        -- W nawiasach kwadratowych musimy dodać pętlę for. W wspomnianej pętli ustalamy na jaką kolumnę patrzymy oraz 
        -- Czy zwiększamy/zmiejszamy zakres
        SALARY[FOR EMPLOYEE_ID FROM 100 TO 105 INCREMENT 2] = 999
    )
/
SELECT 
    EMPLOYEE_ID
    , SALARY
FROM
    EMPLOYEES EMP
MODEL
    DIMENSION BY(EMPLOYEE_ID)
    MEASURES(SALARY)
    RULES
    (
        -- Jeżeli zmniejszamy to uwaga pierwsza liczba musi być mniejsza od drugiej
        SALARY[FOR EMPLOYEE_ID FROM 150 TO 100 DECREMENT 1] = 999
    )
/

-- ENG

-- In the model clause, we can address the range of cells that we will be modifying in the model.

SELECT 
    EMPLOYEE_ID
    , SALARY
FROM
    EMPLOYEES EMP
MODEL
    DIMENSION BY(EMPLOYEE_ID)
    MEASURES(SALARY)
    RULES
    (
        -- In square brackets, we need to add a for loop. In the mentioned loop, we set which column we are looking at and
        -- Whether we are increasing/decreasing the range
        SALARY[FOR EMPLOYEE_ID FROM 100 TO 105 INCREMENT 2] = 999
    )
/
SELECT 
    EMPLOYEE_ID
    , SALARY
FROM
    EMPLOYEES EMP
MODEL
    DIMENSION BY(EMPLOYEE_ID)
    MEASURES(SALARY)
    RULES
    (
        -- If decreasing, note that the first number must be smaller than the second
        SALARY[FOR EMPLOYEE_ID FROM 150 TO 100 DECREMENT 1] = 999
    )
/
