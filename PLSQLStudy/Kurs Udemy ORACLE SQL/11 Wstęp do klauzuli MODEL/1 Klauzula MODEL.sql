/*
    Modele pozwalają na odnoszenie się do konkretych komórek w tabeli. Pozwala to na tworzenie widoków, które innymi metodami sql byłyby cieżkie do osiągnięcia
*/


CREATE TABLE FK_MODEL_STUDY
(
    MODEL_ID INT
    , MODEL_VALUE INT
)
/
INSERT INTO fk_model_study 
    VALUES(11,123)
/
SELECT 
    MODEL_ID
    , MODEL_VALUE
FROM
    fk_model_study
--tutaj zaczynamy kreowanie naszego modelu. Przypomina to trochę tabele dwuwymiarowe z innych języków programowania. 
MODEL
    --tutaj podajemy nazwę kolumny którą będziemy identyfikować wiersz
    DIMENSION BY(MODEL_ID)
    --tutaj natomiast komórkę z wartościami
    MEASURES(MODEL_VALUE)
    --Rules to będą już wiersze które chcemy osiagnąć
    RULES
    (
        MODEL_VALUE[MODEL_ID = 11] = MODEL_VALUE[MODEL_ID = 11] * 123
    )
/

/*
    Models allow for referencing specific cells in a table. This enables the creation of views that would be difficult to achieve using other SQL methods.
*/

CREATE TABLE FK_MODEL_STUDY
(
    MODEL_ID INT
    , MODEL_VALUE INT
)
/
INSERT INTO fk_model_study 
    VALUES(11,123)
/
SELECT 
    MODEL_ID
    , MODEL_VALUE
FROM
    fk_model_study
-- Here we start creating our model. It resembles two-dimensional tables from other programming languages.
MODEL
    -- Here we specify the column that will identify the row
    DIMENSION BY(MODEL_ID)
    -- Here, the cell with values
    MEASURES(MODEL_VALUE)
    -- Rules will be the rows that we want to achieve
    RULES
    (
        MODEL_VALUE[MODEL_ID = 11] = MODEL_VALUE[MODEL_ID = 11] * 123
    )
/
