-- Kiedy w klauzuli model wykorzystujemy pętle for możemy od nieść się do aktualnego rekordu poprzez 
-- funkcje cv()

SELECT 
    EMPLOYEE_ID
    , SALARY
FROM
    EMPLOYEES
MODEL RETURN UPDATED ROWS
    DIMENSION BY(EMPLOYEE_ID)
    MEASURES(SALARY)
    RULES
    (
        SALARY[FOR EMPLOYEE_ID FROM 100 TO 150 INCREMENT 1] = SALARY[CV()]+999
    )
/


SELECT 
    EMPLOYEE_ID
    , SALARY
FROM
    EMPLOYEES
MODEL RETURN UPDATED ROWS
    DIMENSION BY(EMPLOYEE_ID)
    MEASURES(SALARY)
    RULES
    (
        -- są jeszcze inne opcje by określić co powinno zostać zmodyfikowane przez klauzule model np. IN([values])
        SALARY[FOR EMPLOYEE_ID IN(100,101,104)] = SALARY[CV()]+999
    )
/


SELECT 
    EMPLOYEE_ID
    , SALARY
FROM
    EMPLOYEES
MODEL RETURN UPDATED ROWS
    DIMENSION BY(EMPLOYEE_ID)
    MEASURES(SALARY)
    RULES
    (
        SALARY
        [
            -- Można też zastosować podzapytanie ale trzeba zwrócić uwagę że możemy użyć praktycznie tylko 
            -- podzapytania w takiej formie
            FOR EMPLOYEE_ID IN
            (
                SELECT 
                    EMPLOYEE_ID SUB_EMP_ID
                FROM    
                    EMPLOYEES
                WHERE
                    EMPLOYEE_ID BETWEEN
                        100 
                        AND
                            150
            )
        ] = 
            SALARY[CV()]+999
    )
/


-- When using the FOR loop in the MODEL clause, we can refer to the current record using the CV() function.

SELECT 
    EMPLOYEE_ID
    , SALARY
FROM
    EMPLOYEES
MODEL RETURN UPDATED ROWS
    DIMENSION BY(EMPLOYEE_ID)
    MEASURES(SALARY)
    RULES
    (
        SALARY[FOR EMPLOYEE_ID FROM 100 TO 150 INCREMENT 1] = SALARY[CV()]+999
    )
/

SELECT 
    EMPLOYEE_ID
    , SALARY
FROM
    EMPLOYEES
MODEL RETURN UPDATED ROWS
    DIMENSION BY(EMPLOYEE_ID)
    MEASURES(SALARY)
    RULES
    (
        -- There are other options to specify what should be modified by the MODEL clause, e.g., IN([values]).
        SALARY[FOR EMPLOYEE_ID IN(100,101,104)] = SALARY[CV()]+999
    )
/

SELECT 
    EMPLOYEE_ID
    , SALARY
FROM
    EMPLOYEES
MODEL RETURN UPDATED ROWS
    DIMENSION BY(EMPLOYEE_ID)
    MEASURES(SALARY)
    RULES
    (
        SALARY
        [
            -- Subqueries can also be used, but note that this form restricts the use of subqueries.
            FOR EMPLOYEE_ID IN
            (
                SELECT 
                    EMPLOYEE_ID SUB_EMP_ID
                FROM    
                    EMPLOYEES
                WHERE
                    EMPLOYEE_ID BETWEEN
                        100 
                        AND
                            150
            )
        ] = 
            SALARY[CV()]+999
    )
/
