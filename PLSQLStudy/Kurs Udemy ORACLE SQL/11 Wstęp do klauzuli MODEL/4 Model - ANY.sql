-- Tutaj przykład jak możemy wykorzystać model i any by uprościć sobie budowę niektórych zapytań.
-- Chcemy wziąść wartośc pola salary i dodać do niej następną i jeszcze następną wartość

WITH
SUBQUERY AS
(
    SELECT 
        E.EMPLOYEE_ID
        , E.SALARY
        , LEAD(E.SALARY) OVER (ORDER BY E.EMPLOYEE_ID) NEXT_SALARY
        ,FIRST_VALUE(E.SALARY) OVER 
        (
            ORDER BY E.EMPLOYEE_ID
            ROWS BETWEEN
                3 FOLLOWING
                AND
                    UNBOUNDED FOLLOWING
        ) THIRD_NEXT_SALARY
    FROM
        EMPLOYEES E
)
SELECT 
    EMPLOYEE_ID
    , SALARY
    ,THIRD_NEXT_SALARY
    , SALARY + NEXT_SALARY + THIRD_NEXT_SALARY AS "CALCULATED SALARY"
FROM    
    SUBQUERY
ORDER BY 
    EMPLOYEE_ID
FETCH 
    FIRST 10 ROWS ONLY;
/


SELECT 
    EMPLOYEE_ID
    , SALARY
FROM 
    EMPLOYEES
MODEL
    DIMENSION BY(EMPLOYEE_ID)
    MEASURES(SALARY)
    RULES
    (
        SALARY[ANY] ORDER BY EMPLOYEE_ID = 
            SALARY[CV()] + SALARY[CV()+1] + SALARY[CV()+3]
    )
ORDER BY EMPLOYEE_ID
FETCH FIRST 10 ROWS ONLY;
/