/*
    W tej lekcji nauczyłem się trochę o blokach kodu jakie znajdują się w plsql:
        DECLARE -> do deklarowania zmiennych stałych ect.
        BEGIN END -> to takie klamry które trzymają kod miedzy sobą. Tylko one są wymagane podczas programowania
        EXCEPTIONS -> Służą do obsługi logicznych błędów w naszym kodzie (np. jeżeli będziemy dzielić przez 0)
*/


-- poniżej pokazanie jak przedstawiają się bloki kodu w plsql mógłbym jeszcze dodać tutaj blok EXCEPTIONS ale autor kursu zalecał by 
-- zostawić to na dalsze lekcje
DECLARE
BEGIN
    NULL;
END;
/
-- tutaj natomiast uczyłem się o wyświetlaniu przez bazę danych róznych wiadomości by to zrobić musimy najpierw w naszej sesji
-- uruchomić SERVEROUTPUT tak jak zrobiłem to linie niżej
SET SERVEROUTPUT ON;
BEGIN
    -- następnie dzięki temu możemy wyświetlać informacje w kodzie
    DBMS_OUTPUT.put_line('HELLO WORLD');
    BEGIN
        -- BEGIN END pozwala nam tworzyć zagnieżdzenia w kodzie w których możemy wyświetlać kolejne informacje itp.
        DBMS_OUTPUT.put_line('PL/SQL COURSE');
    END;
END;
/