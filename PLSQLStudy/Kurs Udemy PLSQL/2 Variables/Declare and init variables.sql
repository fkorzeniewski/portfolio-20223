/*
    Tutaj małe odświerzenie jak zadeklarować i przypisać wartość do zmiennej.
    Użyłem tutaj też konkatenacji i zaokgrąglenia wartości.
*/


SET SERVEROUTPUT ON;
DECLARE
    V_TEXT VARCHAR2(20) NOT NULL DEFAULT 'THIS IS';
    V_NUMBER NUMBER NOT NULL DEFAULT 50;
    V_NUMBER2 NUMBER(10,2) NOT NULL DEFAULT 50.222;
BEGIN
    v_text := v_text || ' BEGGINER';
    v_number := ROUND
    (
        (v_number/3) * v_number2
        , 2
    );
    DBMS_OUTPUT.PUT_LINE(V_TEXT || ' PLSQL COURSE');
    DBMS_OUTPUT.PUT_LINE(V_NUMBER || ' <= TEST NUMBER');
    DBMS_OUTPUT.PUT_LINE(V_NUMBER2 || ' <= TEST NUMBER2');
END;
/