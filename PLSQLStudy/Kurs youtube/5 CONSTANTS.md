# CONSTANTS

<p>
    W PL/SQL (Procedural Language/SQL), CONSTANTS to stałe, które są niezmiennymi wartościami przypisanymi zmiennym w czasie ich deklaracji. Stosowanie stałych pozwala na zwiększenie czytelności kodu, ułatwia jego utrzymanie i redukuje ryzyko błędów.
    Definiowanie stałych W PL/SQL stałe definiuje się w sekcji deklaracyjnej bloku PL/SQL, pakietu, procedury lub funkcji.
</p>


```sql
    CONSTANT constant_name constant_type := constant_value;
```

Przykład użycia stałej

```sql
    SET SERVEROUTPUT ON;


    DECLARE
        pi CONSTANT NUMBER := 3.14159;
        max_salary CONSTANT NUMBER := 100000;
    BEGIN
        DBMS_OUTPUT.PUT_LINE(pi);
        DBMS_OUTPUT.PUT_LINE(max_salary);
    END;
```