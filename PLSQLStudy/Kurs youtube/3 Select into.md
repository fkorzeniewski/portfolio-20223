# _SELECT INTO_

<p>
W PL/SQL klauzula SELECT INTO jest używana do przypisywania wyników zapytania SQL do zmiennych PL/SQL. Klauzula ta umożliwia przechwycenie wyników zapytania bezpośrednio do zmiennych, które mogą być następnie używane w dalszym kodzie PL/SQL. Poniżej znajduje się ogólna składnia oraz przykłady użycia klauzuli SELECT INTO.
</p>

```sql
    SELECT kolumna1, kolumna2, ...
    INTO zmienna1, zmienna2, ...
    FROM tabela
    WHERE warunki;
```

Zapiszę teraz do zmiennej nazwisko pracownika którego id jest równe 100

```sql
    SET SERVEROUTPUT ON;


    DECLARE
        v_emp_name VARCHAR2(20);
    BEGIN
        SELECT
            NAZWISKO INTO v_emp_name
        FROM
            PRACOWNICY
        WHERE
            ID_PRAC = 100;
            
            
        --WYŚWIETLANIE WYNIKÓW
        DBMS_OUTPUT.PUT_LINE(v_emp_name);
    END;
```

Poniżej program, który dodatkowo zapiszę jego pensję

```sql
    SET SERVEROUTPUT ON;


    DECLARE
        v_emp_name VARCHAR2(20);
        v_emp_salary NUMBER(10,2);
    BEGIN
        SELECT
            NAZWISKO , (Placa_POD + NVL(Placa_DOD,0)) 
            INTO v_emp_name,v_emp_salary
        FROM
            PRACOWNICY
        WHERE
            ID_PRAC = 100;
            
            
        --WYŚWIETLANIE WYNIKÓW
        DBMS_OUTPUT.PUT_LINE(v_emp_name);
        DBMS_OUTPUT.PUT_LINE(v_emp_salary);
    END;
```


Na koniec dodam jeszcze obługę wyjątków

```sql
    SET SERVEROUTPUT ON;


    DECLARE
        v_emp_name VARCHAR2(20);
        v_emp_salary NUMBER(10,2);
    BEGIN
        SELECT
            NAZWISKO , (Placa_POD + NVL(Placa_DOD,0)),ID_PRAC 
            INTO v_emp_name,v_emp_salary
        FROM
            PRACOWNICY
        WHERE
            ID_PRAC = 100;
            
            
        --WYŚWIETLANIE WYNIKÓW
        DBMS_OUTPUT.PUT_LINE(v_emp_name);
        DBMS_OUTPUT.PUT_LINE(v_emp_salary);
        
        
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            DBMS_OUTPUT.PUT_LINE('No employee found with the given ID.');
        WHEN TOO_MANY_ROWS THEN
            DBMS_OUTPUT.PUT_LINE('Multiple employees found with the given ID.');
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('An unexpected error occurred: ' || SQLERRM);
    END;
    ```