# While loop

<p>
    Pętla while powtarza blok kodu dopuki warunek z jego deklaracji nie zostanie spełniony. Jest to dobra pętlą do walidacji danych. Ponieważ może ona się powtarzać "w nieskończoność"
</p>

```sql
    SET SERVEROUTPUT ON;


    DECLARE
        -- Deklaracja zmiennych
        counter NUMBER := 1;  -- Zmienna licznikowa
    BEGIN
        -- Pętla WHILE
        WHILE counter <= 10 LOOP
            -- Blok kodu do wykonania
            DBMS_OUTPUT.PUT_LINE('Counter: ' || counter);

            -- Zwiększenie wartości licznika
            counter := counter + 1;
        END LOOP;
    END;
```

<p>
Pętla WHILE w PL/SQL powtarza blok kodu, dopóki określony warunek jest spełniony. Składa się z deklaracji zmiennych, warunku pętli sprawdzanego przed każdą iteracją oraz ciała pętli, które zawiera kod wykonywany w każdej iteracji. Zmienna używana w warunku jest zazwyczaj modyfikowana w ciele pętli, aby zapewnić, że pętla ostatecznie zakończy działanie. Na przykład, pętla może zwiększać wartość licznika i wyświetlać ją, dopóki licznik nie osiągnie określonej wartości.
</p>