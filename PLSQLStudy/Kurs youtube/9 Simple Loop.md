# Simple Loop

pętle pozwalają na wielokrotne wykonywanie określonego bloku kodu. Jednym z najprostszych rodzajów pętli jest Simple Loop (prosta pętla). Poniżej znajduje się szczegółowy opis tej konstrukcji:


```sql
SET SERVEROUTPUT ON;
DECLARE
    vCount NUMBER := 0;
BEGIN
    LOOP
        vCount := vCount + 1;
        DBMS_OUTPUT.PUT_LINE('Suma to: ' || vCount);
        EXIT WHEN vCount >= 10;
    END LOOP;
END;
```


PL/SQL Simple Loop to prosta, ale potężna konstrukcja do wykonywania iteracji w bazach danych Oracle. Pozwala na elastyczne i kontrolowane wykonywanie kodu do momentu spełnienia określonego warunku. Jest to fundamentalne narzędzie w arsenale programisty PL/SQL, szczególnie przydatne w skryptach, które wymagają powtarzalnych operacji.