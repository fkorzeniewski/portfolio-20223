# %TYPE

<p>
    W PL/SQL, %TYPE jest atrybutem służącym do definiowania zmiennych, które mają ten sam typ danych jak istniejące kolumny w bazie danych lub jak inne zmienne PL/SQL. Dzięki temu atrybutowi, zmienne automatycznie dziedziczą typ danych kolumny lub zmiennej, co ułatwia zarządzanie typami danych i zwiększa elastyczność kodu.
</p>

```sql
    DECLARE
        v_salary employees.salary%TYPE;
    BEGIN
        SELECT salary INTO v_salary
        FROM employees
        WHERE employee_id = 101;
        DBMS_OUTPUT.PUT_LINE('Salary: ' || v_salary);
    END;
```

Kod który stworzy zmienną v_surname nada jej taki typ jak nazwisko w tabeli PRACOWNICY a na koniec je pobierze i wyświetli


```sql
    SET SERVEROUTPUT ON;

    DECLARE
        v_surname PRACOWNICY.NAZWISKO%TYPE;
    BEGIN
        SELECT 
            NAZWISKO INTO v_surname
        FROM
            PRACOWNICY
        WHERE 
            ID_PRAC = 100;
        DBMS_OUTPUT.PUT_LINE(v_surname);
    END;
```