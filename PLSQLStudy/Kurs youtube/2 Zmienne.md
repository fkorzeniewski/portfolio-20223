# Zmienne

<p>
    Zmienne zostały poruszone już przezemie w porzednim pliku. Służa one tak jak w innych językach programowania do przechowywania oraz manipulowania danymi.
</p>

# Delkaracja zmiennych

```sql
    variable_name [CONSTANT] datatype [NOT NULL] [:= initial_value];
```

ponownie wzór na stworzenie zmiennej i teraz praktyka

## Skrypt deklarujący zmienną który przypisuje jej wartość w kodzie a nie przy dejkaracji

```sql
    SET SERVEROUTPUT ON;
    DECLARE
        v_name VARCHAR2(20);
    BEGIN
        v_name := 'Franciszek';
        DBMS_OUTPUT.PUT_LINE(v_name);
    END;
```

## Skrypt który utworzy zmienną name i doda do niej drugie imie


```sql
    SET SERVEROUTPUT ON;
    DECLARE
        v_name VARCHAR2(20);
    BEGIN
        v_name := 'Franciszek';
        DBMS_OUTPUT.PUT_LINE(v_name);
        v_name := v_name || ' Tomasz';
        DBMS_OUTPUT.PUT_LINE(v_name);
    END;
```

## Deklaracja różnych typów danych w zmiennych

```sql
    DECLARE
        employee_id NUMBER;  -- Deklaracja zmiennej liczbowej
        employee_name VARCHAR2(100);  -- Deklaracja zmiennej tekstowej
        hire_date DATE := SYSDATE;  -- Deklaracja zmiennej daty z inicjalizacją bieżącą datą
    BEGIN
        DBMS_OUTPUT.PUT_LINE(employee_id);
        employee_name := 'Kasia';
        DBMS_OUTPUT.PUT_LINE(employee_name);
        DBMS_OUTPUT.PUT_LINE(hire_date);
    END;
```

## Typy złożone
    
- Rekordy - struktury danych pozwalające na przechowywanie różnych typów danych w jednym obiekcie.
- Tablice asocjacyjne - dynamiczne tablice indeksowane wartościami kluczy.
- Typy obiektowe - zdefiniowane przez użytkownika typy danych.

```sql
    SET SERVEROUTPUT ON;
    DECLARE
        TYPE EmployeeRec IS RECORD (
            emp_id NUMBER,
            emp_name VARCHAR2(100),
            emp_salary NUMBER
        );
        employee EmployeeRec;  -- Deklaracja zmiennej rekordu
        
        
        TYPE NumberTable IS TABLE OF NUMBER INDEX BY PLS_INTEGER;
        salaries NumberTable;  -- Deklaracja tablicy asocjacyjnej
    BEGIN
        -- Przypisanie wartości do zmiennej rekordu
        employee.emp_id := 1001;
        employee.emp_name := 'John Doe';
        employee.emp_salary := 75000;

        -- Wyświetlenie wartości zmiennej rekordu
        DBMS_OUTPUT.PUT_LINE('Employee ID: ' || employee.emp_id);
        DBMS_OUTPUT.PUT_LINE('Employee Name: ' || employee.emp_name);
        DBMS_OUTPUT.PUT_LINE('Employee Salary: ' || employee.emp_salary);

        -- Przypisanie wartości do tablicy asocjacyjnej
        salaries(1) := 50000;
        salaries(2) := 60000;
        salaries(3) := 70000;

        -- Wyświetlenie wartości z tablicy asocjacyjnej
        -- używają pętli for
        FOR i IN 1..3 LOOP
            DBMS_OUTPUT.PUT_LINE('Salary ' || i || ': ' || salaries(i));
        END LOOP;
    END;
```