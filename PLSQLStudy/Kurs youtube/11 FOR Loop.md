```sql
BEGIN
    FOR i IN 1..10 LOOP
        IF i = 5 THEN
            -- Operacja do wykonania w 5 iteracji
            DBMS_OUTPUT.PUT_LINE('Special operation at iteration: ' || i);
        ELSE
            -- Operacja do wykonania w pozostałych iteracjach
            DBMS_OUTPUT.PUT_LINE('Regular operation at iteration: ' || i);
        END IF;
    END LOOP;
END;
```