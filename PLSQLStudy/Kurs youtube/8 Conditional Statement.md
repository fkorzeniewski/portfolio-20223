# Conditional Statement

Conditional statements (instrukcje warunkowe) w PL/SQL pozwalają na 
sterowanie przepływem programu w zależności od określonych warunków. 
Główne typy instrukcji warunkowych to:

- **IF-THEN-ELSE Statement**

- **Case Statement**

```sql
SET SERVEROUTPUT ON;
DECLARE
    liczba NUMBER := 5;
BEGIN
    IF liczba < 10 THEN
        DBMS_OUTPUT.PUT_LINE('Liczba jest mniejsza niż 10');
    ELSIF liczba = 10 THEN
        DBMS_OUTPUT.PUT_LINE('Liczba jest równa 10');
    ELSE
        DBMS_OUTPUT.PUT_LINE('Liczba jest większa niż 10');
    END IF;
END;
```

```sql
SET SERVEROUTPUT ON;
DECLARE
    ocena CHAR(1) := 'A';
BEGIN
    CASE ocena
        WHEN 'A' THEN
            DBMS_OUTPUT.PUT_LINE('Świetnie');
        WHEN 'B' THEN
            DBMS_OUTPUT.PUT_LINE('Dobrze');
        WHEN 'C' THEN
            DBMS_OUTPUT.PUT_LINE('Może być');
        ELSE
            DBMS_OUTPUT.PUT_LINE('Nieznana ocena');
    END CASE;
END;
```

Instrukcje warunkowe są kluczowym elementem PL/SQL, umożliwiającym dynamiczne sterowanie wykonaniem programu. Zarówno `IF-THEN-ELSE`, jak i `CASE` oferują różne podejścia do realizacji logiki warunkowej, pozwalając na 
elastyczne i czytelne zarządzanie przepływem sterowania w kodzie.
