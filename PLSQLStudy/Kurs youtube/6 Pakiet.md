# Pakiety

<p>
    W PL/SQL (Procedural Language/Structured Query Language) pakiet jest zaawansowaną konstrukcją służącą do grupowania logicznie powiązanych typów danych, zmiennych, stałych, procedur i funkcji w jednym, zorganizowanym module. Pakiety są używane do zwiększenia modularności i organizacji kodu PL/SQL, co ułatwia zarządzanie dużymi aplikacjami i ponowne wykorzystanie kodu.
</p>

<p>
    Na początek musimy stworzyć sprcyfikacje pakietu czyli ustalić jakie będzie zawierała w sobie procedury czy też funkcje
</p>

```sql
    CREATE OR REPLACE PACKAGE StudyPackage IS
        PROCEDURE StudyProcedure;
        FUNCTION StudyFunction return varchar2;
    END STudyPackage;
```

<p>
    Następnie deklarujemy body tego pakietu czyli tak naprawdę to co dana funkcja/procedura będzie faktycznie robić
</p>

```SQL
    CREATE OR REPLACE PACKAGE BODY StudyPackage IS
        
        
        PROCEDURE StudyProcedure IS
        BEGIN
            UPDATE 
                PRACOWNICY
            SET 
                PLACA_POD = 10
            WHERE
                ID_PRAC = 999;
                
                
            COMMIT;
        END StudyProcedure;
        
        
        FUNCTION StudyFunction RETURN VARCHAR2 IS
        BEGIN
            return 'test';
        END StudyFunction;
        
        
    END StudyPackage;
```

<p>
    Przykładowe wykorzystanie procedury z pakietu
</p>

```sql
    BEGIN
        StudyPackage.studyprocedure;
    END;
```

```sql
    SET SERVEROUTPUT ON;


    DECLARE
    v_test VARCHAR2(50);
    BEGIN
    v_test := StudyPackage.studyfunction;
    DBMS_OUTPUT.PUT_LINE(v_test);
    END;
```