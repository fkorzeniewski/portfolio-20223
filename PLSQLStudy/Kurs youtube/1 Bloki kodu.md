## Bloki PL/SQL


<p>
    Bloki to podstawowa jednostka programowania podzielić je można na trzy sekcje
</p>


- Deklaracjna
- Wykonawcza
- Obsługi wyjątków
  

## Deklaracyjna


```sql
DECLARE 
    variable_name datatype := value;
```


Powyżej przedstawiłem wzór na zadeklarowanie zmiennej _DECLARE_ rozpoczyna deklaracje zmiennej potem nazwa _variable_name_, _datatype_ typ zmiennej _:= value_ przypisanie wartości

```sql
DECLARE
    var_age integer := 12;
```

powyżej zadeklarowałem zmienną _var_age_ z wartością _12_ 


## Wykonawcza

```sql
SET SERVEROUTPUT ON;

DECLARE
    var_age INTEGER := 12;
BEGIN
    dbms_output.put_line(var_age);
END;
```

Wykonawczy to blok, który wykonuje się w nim operacje.
W kodzie powyżej wyświetliłem wartość mojej zmiennej

```sql
SET SERVEROUTPUT ON;


BEGIN
    dbms_output.put_line(12);
END;
```


## Obsługa wyjątków

```sql
SET SERVEROUTPUT ON;

BEGIN
    dbms_output.put_line(12 / 0);
EXCEPTION
    WHEN ZERO_DIVIDE THEN
        dbms_output.put_line('blad dzielenia przez zero');
END;
```

Powyższy kod przedstawia działanie bloku _exception_ możemy dzięki niemu wykonywać operacje gdy wystąpi konkretny bład.