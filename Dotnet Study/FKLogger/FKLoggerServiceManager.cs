﻿using NLog;
using NLog.LayoutRenderers;
using Xunit;


namespace FKLogger;


public class FKLoggerServiceManager : FKLoggerServiceBase
{
    [Fact]
    public override void Logger_Test()
    {
        bool result = false;
        string message = string.Empty;


        try
        {
            result = true;
        }
        catch (Exception e)
        {
            message = $"{e.Message}\n{e.StackTrace}";
            result = false;
        }


        Assert.True(result, $"[TEST Logger_Test]\n\n=>{result}<=\n\n{message}");
    }
}
