using NLog;
using NLog.Config;
using NLog.Targets;
using Xunit;


namespace FKLogger;


public abstract class FKLoggerServiceBase : IFKLoggerService
{
    public Logger FKLogger { get; }


    public FKLoggerServiceBase()
    {
        FKLogger = ConfigLogger();


        //test logger
        Logger_Test();
    }


    [Fact]
    public abstract void Logger_Test();


    private Logger ConfigLogger()
    {
        var config = new LoggingConfiguration();

        //file targets
        var fileTarget = new FileTarget("FileTarget")
        {
            FileName = @"${basedir}\LOGS\${shortdate} Logs.log",
            Layout = "${shortdate} ${message}"
        };
        var infoTarget = new FileTarget("infoTarget")
        {
            FileName = @"${basedir}\LOGS\${shortdate} InfoLogs.log",
            Layout = "${shortdate} ${message}"
        };
        var warningTarget = new FileTarget("warningTarget")
        {
            FileName = @"${basedir}\LOGS\${shortdate} WarningLogs.log",
            Layout = "${shortdate} ${message}"
        };
        var errorTarget = new FileTarget("errorTarget")
        {
            FileName = @"${basedir}\LOGS\${shortdate} ErrorLogs.log",
            Layout = "${shortdate} ${message}"
        };



        //console target
        ColoredConsoleTarget consoleTarget = new("LogConsole")
        {
            Layout = "${longdate} ${level:uppercase=true} ${message}",
        };


        config.AddTarget("FileTarget", fileTarget);
        config.AddTarget("InfoTarget", infoTarget);
        config.AddTarget("WarningTarget", warningTarget);
        config.AddTarget("ErrorTarget", errorTarget);
        config.AddTarget("ConsoleTarget", consoleTarget);


        config.AddRule(LogLevel.Debug, LogLevel.Fatal, fileTarget);
        config.AddRule(LogLevel.Debug, LogLevel.Info, infoTarget);
        config.AddRule(LogLevel.Warn, LogLevel.Warn, warningTarget);
        config.AddRule(LogLevel.Error, LogLevel.Fatal, errorTarget);
        config.AddRule(LogLevel.Debug, LogLevel.Fatal, consoleTarget);


        LogManager.Configuration = config;


        return LogManager.GetLogger("FKLogger");
    }
}