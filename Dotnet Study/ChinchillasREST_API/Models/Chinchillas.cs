namespace ChinchillaAPI.Models;


public class Chinchilla
{
    public string? ID { get; set; }
    public string? FurColor { get; set; }
    public string? Name { get; set; }
    public int Age { get; set; }
}
