using ChinchillaAPI.DBContext;
using ChinchillaAPI.Models;


namespace ChinchillaAPI.Repository;


public class ChinchillaRepository
{
    private readonly ChinchillaDbContext _context;


    public ChinchillaRepository()
    {
        _context = new ChinchillaDbContext();
        _context.Database.EnsureCreated();
    }


    public void AddChinchilla(Chinchilla chinchilla)
    {
        _context.Chinchillas.Add(chinchilla);
        _context.SaveChanges();
    }


    public void RemoveChinchilla(string ID)
    {
        var toRemove = _context.Chinchillas.FirstOrDefault(x => x.ID == ID);
        if(toRemove == null)
            throw new NullReferenceException();

        
        _context.Chinchillas.Remove(toRemove);
    }


    public void UpdateChinchilla(Chinchilla chin)
    {
        var chinToUpdate = _context.Chinchillas.FirstOrDefault(x=> x.ID == chin.ID);


        if(chinToUpdate == null)
            return;


        chinToUpdate = chin;
        _context.SaveChanges();
    }


    public IQueryable<Chinchilla> GetAllChinchillas()
    {
        return _context.Chinchillas.AsQueryable();
    }
}