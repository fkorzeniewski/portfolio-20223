using ChinchillaAPI.Models;
using ChinchillaAPI.Repository;

namespace ChinchillaAPI;


public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);


        // Add services to the container.
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();


        var app = builder.Build();


        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }


        var chinchillaRepo = new ChinchillaRepository();


        app.UseHttpsRedirection();


        app.MapGet("/GetChinchillas", () =>
        {


            var Chinchillas = chinchillaRepo.GetAllChinchillas();


            return Chinchillas.ToList();
        })
        .WithName("GetChinchillas")
        .WithOpenApi();


        app.MapPost("/AddChinchillas",(List<Chinchilla> chinList)=>
        {
            foreach(Chinchilla newChin in chinList)
                chinchillaRepo.AddChinchilla(newChin);
        })
        .WithName("AddChinchillas")
        .WithOpenApi();


        app.MapDelete("/DeleteChinchillas",(string ID)=>
        {
            chinchillaRepo.RemoveChinchilla(ID);
        })
        .WithName("DeleteChinchillas")
        .WithOpenApi();


        app.MapPut("/UpdateChinchilla",(Chinchilla chin)=>
        {
            chinchillaRepo.UpdateChinchilla(chin);
        })
        .WithName("UpdateChinchilla")
        .WithOpenApi();


        app.Run();
    }
}


