using Microsoft.EntityFrameworkCore;
using ChinchillaAPI.Models;


namespace ChinchillaAPI.DBContext;


public class ChinchillaDbContext : DbContext
    {
        public DbSet<Chinchilla> Chinchillas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=./Chinchillas.db");
        }
    }