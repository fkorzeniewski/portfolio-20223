﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleAES;


namespace Cipher
{
    public class FraKorCipher
    {
        private readonly string _key = "7656543!@#!%76543432178ovcdser34567wertyui23456^%$#@#$";


        public string Encrypt(string input) => SimpleAES.AES256.Encrypt(input,_key);
        public string Decrypt(string input) => SimpleAES.AES256.Decrypt(input,_key);
    }
}
