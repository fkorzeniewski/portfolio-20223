using CipherForm.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Cipher;
using System.Reflection.Metadata.Ecma335;


namespace CipherForm.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly FraKorCipher _cipher;


        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
            _cipher = new FraKorCipher();
        }


        public IActionResult Index()
        {
            return View();
        }


        public IActionResult Privacy()
        {
            return View();
        }


        public IActionResult AboutMe() => View();


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        //return encrypted password
        [HttpPost]
        public IActionResult EncryptPasswd(string toEncrpt)
        {
            var enctyptedPasswd = _cipher.Encrypt(toEncrpt);


            ViewBag.ToEncrpt = enctyptedPasswd;
            return View("Index");
        }
    }
}
