﻿using Integrator.Abstraction;
using Integrator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Xml.Linq;

namespace Integrator.Models
{
    public class Chinchilla : RodentBase
    {
        //Because chinchillas are too good to bite you
        public override bool Bite(out string mess)
        {
            mess = string.Empty;
            return false;
        }
    }
}
