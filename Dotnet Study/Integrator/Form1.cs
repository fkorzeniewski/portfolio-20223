﻿using Integrator.Models;
using Integrator.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;


namespace Integrator
{
    public partial class Integrator : Form
    {
        private List<Chinchilla> _chinchillas = new List<Chinchilla>();
        private XMLService _xmlService;
        private JSONService _jsonService;
        private RestService _restService;


        public Integrator()
        {
            InitializeComponent();
        }


        private bool InitDataGridView(out string mess)
        {
            mess = string.Empty;


            try
            {
                dataGridView1.DataSource = _chinchillas;
                dataGridView1.AutoResizeColumns();
                dataGridView1.Refresh();


                return true;
            }
            catch (Exception e)
            {
                mess = $"{e.Message} \n {e.StackTrace}";
                dataGridView1.Enabled = false;


                return false;
            }
        }


        private void Integrator_Load(object sender, EventArgs e)
        {
            //version
            var assembly = Assembly.GetExecutingAssembly();
            this.Text = $"Integrator {assembly.GetName().Version}";


            //services
            _xmlService = new XMLService("");
            _jsonService = new JSONService("");
            _restService = new RestService("dd", "https://example.com/api");


            //init datagrid
            if (!InitDataGridView(out string mess))
                MessageBox.Show(mess, "InitDataGridView", MessageBoxButtons.OK, MessageBoxIcon.Error);


            //backgroundworker
            UploadChinchillasBW.ProgressChanged += UploadChinchillasBW_ProgressChanged;
            UploadChinchillasBW.WorkerReportsProgress = true;
            UploadChinchillasBW.RunWorkerCompleted += UploadChinchillasBW_RunWorkerCompleted;


            openFileDialog1.Filter = "Chinchillas XML file |*.xml";
        }


        #region Buttons


        private void UploadFileBtnClick(object sender, EventArgs e)
        {
            if (!UploadFile(out string mess))
                MessageBox.Show(mess, "UploadFile error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        private void GetChinchillasButtonClick(object sender, EventArgs e)
        {

        }


        private void ExportFileBtnClick(Object sender, EventArgs e) 
        {
            if(_chinchillas.Count == 0)
            {
                MessageBox.Show("No Chinchillas to export", "ExportChinchillaFile warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ExportFile.Enabled = false;


                return;
            }else
                ExportFile.Enabled = true;


            if (!ExportChinchillaFile(out string mess))
                MessageBox.Show(mess, "ExportChinchillaFile error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        private void ExportJSONFileBtnClick(Object sender, EventArgs e)
        {
            if (_chinchillas.Count == 0)
            {
                MessageBox.Show("No Chinchillas to export", "ExportChinchillaFile warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ExportJson.Enabled = false;


                return;
            }
            else
                ExportJson.Enabled = true;


            if (!ExportJSONChinchillaFile(out string mess))
                MessageBox.Show(mess, "ExportChinchillaFile error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        private void SendChinchillasBtnClick(object sender, EventArgs e)
        {
            //todo
        }



        #endregion




        private bool UploadFile(out string mess)
        {
            mess = string.Empty;


            try
            {
                var dialogResult = openFileDialog1.ShowDialog();


                if (dialogResult != DialogResult.OK)
                    return true;


                label1.ForeColor = Color.Green;
                label1.Text = openFileDialog1.FileName;


                if (!UploadChinchillasBW.IsBusy)
                    UploadChinchillasBW.RunWorkerAsync();


                return true;
            }
            catch (Exception e)
            {
                mess = $"{e.Message} \n {e.StackTrace}";
                return false;
            }
        }


        private bool ExportChinchillaFile(out string mess)
        {
            mess = string.Empty;


            try
            {
                saveFileDialog1.Filter = "Chinchillas XML file |*.xml";
                var dialogResult = saveFileDialog1.ShowDialog();


                if (dialogResult != DialogResult.OK)
                    return true;


                if(!_xmlService.ExportChinchillas(out mess,saveFileDialog1.FileName,_chinchillas))
                    MessageBox.Show(mess, "UploadChinchillas error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    MessageBox.Show($"Exportet to {saveFileDialog1.FileName}", "UploadChinchillas info", MessageBoxButtons.OK, MessageBoxIcon.Information);


                return true;
            }
            catch (Exception e)
            {
                mess = $"{e.Message} \n {e.StackTrace}";
                return false;
            }
        }


        private bool ExportJSONChinchillaFile(out string mess)
        {
            mess = string.Empty;


            try
            {
                saveFileDialog1.Filter =  "Chinchillas JSON file |*.json";
                var dialogResult = saveFileDialog1.ShowDialog();


                if (dialogResult != DialogResult.OK)
                    return true;


                if (!_jsonService.ExportChinchillasList(out mess, saveFileDialog1.FileName, _chinchillas))
                    MessageBox.Show(mess, "ExportChinchillasList error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    MessageBox.Show($"Exportet to {saveFileDialog1.FileName}", "ExportChinchillasList info", MessageBoxButtons.OK, MessageBoxIcon.Information);


                return true;
            }
            catch (Exception e)
            {
                mess = $"{e.Message} \n {e.StackTrace}";
                return false;
            }
        }


        #region BackgroundWorker Upload Chinchillas


        private void UploadChinchillasBW_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            //progressBar1.Value = e.ProgressPercentage;
        }


        private void UploadChinchillasBW_DoWork(object sender, DoWorkEventArgs e)
        {
            _xmlService.XMLPath = openFileDialog1.FileName;


            if (!_xmlService.UploadChinchillas(out string mess, out _chinchillas))
                MessageBox.Show(mess, "UploadChinchillas error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        private void UploadChinchillasBW_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            dataGridView1.DataSource = _chinchillas;
            dataGridView1.Refresh();


            ExportFile.Enabled = true;
            ExportJson.Enabled = true;
        }


        #endregion


        #region BackgroundWorker Export Chinchillas


        //todo


        #endregion
    }
}
