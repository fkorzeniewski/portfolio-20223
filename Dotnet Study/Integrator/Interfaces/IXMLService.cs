﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integrator.Interfaces
{
    internal interface IXMLService
    {
        string XMLPath { get; set; }
    }
}
