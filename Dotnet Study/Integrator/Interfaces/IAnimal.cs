﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integrator.Interfaces
{
    internal interface IAnimal
    {
        string ID { get; set; }
        string Name { get; set; }
        int Age { get; set; }


        bool MakeSound(out string mess, out string sound);
    }
}
