﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;


namespace Integrator.Interfaces
{
    internal interface IRestService
    {
        string Token { get; set; }
        RestClient Client { get; set; }


        bool CreateBaseRequest(out string mess,out RestRequest request);
    }
}
