﻿using Integrator.Interfaces;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integrator.Abstraction
{
    internal abstract class RestServiceBase : IRestService
    {
        public string Token { get => _token; set => _token = value; }
        public RestClient Client { get => _client; set => _client = value; }


        private string _token;
        private RestClient _client;


        public RestServiceBase(string token,string BaseAPIAddress)
        {
            _token = token;
            _client = new RestClient(BaseAPIAddress);
        }

        public abstract bool CreateBaseRequest(out string mess, out RestRequest request);
    }
}
