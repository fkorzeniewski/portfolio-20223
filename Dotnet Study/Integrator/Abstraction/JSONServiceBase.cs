﻿using Integrator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integrator.Abstraction
{
    internal abstract class JSONServiceBase : IJSONService
    {
        public string JsonPath { get => _jsonPath; set => _jsonPath = value; }


        private string _jsonPath;


        public JSONServiceBase(string jsonPath)
        {
            _jsonPath = jsonPath;
        }
    }
}
