﻿using Integrator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integrator.Abstraction
{
    internal abstract class XMLServiceBase : IXMLService
    {
        public string XMLPath { get => _xmlPath; set => _xmlPath = value; }


        private string _xmlPath;


        public XMLServiceBase( string xmlPath) => _xmlPath = xmlPath;
    }
}
