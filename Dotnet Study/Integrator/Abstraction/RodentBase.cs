﻿using Integrator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integrator.Abstraction
{
    public abstract class RodentBase : IRodent
    {
        public string ID { get => _id; set => _id = value; }
        public string FurColor {get => _furColor; set => _furColor = value; }
        public string Name { get => _name; set => _name = value; }
        public int Age { get => _age; set => _age = value; }

        private string _furColor;
        private string _name;
        private int _age;
        private string _id;


        public virtual bool Bite(out string mess)
        {
            mess = string.Empty;


            try
            {
                var rdm = new Random();


                if(rdm.Next(0,2) == 0)
                    return false;
                else
                    return true;
            }
            catch (Exception e)
            {
                mess = $"{e.Message} \n {e.StackTrace}";
                return false;
            }
        }


        public virtual bool MakeSound(out string mess, out string sound)
        {
            mess = string.Empty;
            sound = string.Empty;


            try
            {
                string[] rodentSounds =
    {
                    "Squeaking or squealing",
                    "Chattering or chittering",
                    "Gnawing or chewing",
                    "Purring",
                    "Hissing",
                    "Whimpering or whimpering"
                };
                var rdm = new Random();


                sound = rodentSounds[rdm.Next(0, rodentSounds.Length)];
                return true;
            }
            catch (Exception e)
            {
                mess = $"{e.Message} \n {e.StackTrace}";
                return false;
            }
        }
    }
}
