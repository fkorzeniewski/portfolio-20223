﻿using Integrator.Abstraction;
using Integrator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;


namespace Integrator.Services
{
    internal class XMLService : XMLServiceBase
    {
        public XMLService(string xmlPath) : base(xmlPath)
        {
        }


        public bool UploadChinchillas(out string mess,out List<Chinchilla> chinchillas)
        {
            mess = string.Empty;
            chinchillas = new List<Chinchilla>();


            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<Chinchilla>));
                FileStream reader = File.OpenRead(XMLPath);
                var x = (List<Chinchilla>)serializer.Deserialize(reader);


                chinchillas = x;

                return true;
            }
            catch (Exception e)
            {
                mess = $"{e.Message} \n {e.StackTrace}";
                return false;
            }
        }


        public bool ExportChinchillas(out string mess, string exportTo,List<Chinchilla> chinchillas)
        {
            mess = string.Empty;


            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<Chinchilla>));


                using (FileStream fs = new FileStream(exportTo, FileMode.Create))
                    serializer.Serialize(fs, chinchillas);


                return true;                
            }
            catch (Exception e)
            {
                mess = $"{e.Message} \n {e.StackTrace}";
                return false;
            }
        }
    }
}
