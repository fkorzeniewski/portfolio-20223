﻿using Integrator.Abstraction;
using Integrator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;


namespace Integrator.Services
{
    internal class JSONService : JSONServiceBase
    {
        public JSONService(string jsonPath) : base(jsonPath)
        {
        }


        public bool ExportChinchillasList(out string mess,string jsonPath,List<Chinchilla> chinchillas)
        {
            mess = string.Empty;


            try
            {
                // Open a file for writing
                using (FileStream fs = new FileStream(jsonPath, FileMode.Create))
                {
                    // Serialize the list to JSON and write it to the file
                    JsonSerializer.Serialize(fs, chinchillas);
                }
                return true;
            }
            catch (Exception e)
            {
                mess = $"{e.Message} \n {e.StackTrace}";
                return false;
            }
        }
    }
}
