﻿using Integrator.Abstraction;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integrator.Services
{
    internal class RestService : RestServiceBase
    {
        public RestService(string token, string BaseAPIAddress) : base(token, BaseAPIAddress)
        {
        }


        public override bool CreateBaseRequest(out string mess, out RestRequest request)
        {
            mess = string.Empty;
            request = null;


            try
            {
                return true;
            }
            catch (Exception e)
            {
                mess = $"{e.Message} \n {e.StackTrace}";
                return false;
            }
        }
    }
}
