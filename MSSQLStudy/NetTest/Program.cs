﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;


namespace NetTest;


public class ChinchillaContext : DbContext
{
    public DbSet<Chinchilla> Chinchillas { get; set; }
    public DbSet<ChinchillaOwner> ChinchillaOwners { get; set;}


    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var connStr = $@"Server=DESKTOP-IKGQD7V\SQLEXPRESS;Database=ChinchillaShelter;User Id=sa;Password=franek111;TrustServerCertificate=True;";
        optionsBuilder.UseSqlServer(connStr);
    }
}


public class Chinchilla
{
    [Key]
    public int chi_ID { get; set; }
    public string? chi_Name { get; set; }


    public override string ToString() =>
        Newtonsoft.Json.JsonConvert.SerializeObject(this);
}


public class ChinchillaOwner
{
    [Key]
    public int own_ID { get; set; }
    public int own_ChinId { get; set; }
    public string? own_Name { get; set; }


    public override string ToString() =>
        Newtonsoft.Json.JsonConvert.SerializeObject(this);
}


class Program
{
    static void Main()
    {
        string[] names = { "Bella", "Charlie", "Daisy", "Luna", "Milo", "Oliver", "Simba", "Nala", "Leo", "Coco", "Lily", "Max", "Buddy", "Rocky", "Zoe", "Oscar", "Molly", "Toby", "Ruby", "Jack" };
        Random random = new Random();


        using (var context = new ChinchillaContext())
        {

            //remove random chinchilla
            var randomID = random.Next(context.Chinchillas.Count()-1);
            var chinchillaToRemove =
                 context.Chinchillas.FirstOrDefault(x => x.chi_ID == randomID);
            if (chinchillaToRemove == null)
                throw new NullReferenceException("chinchillaToRemove is null");


            Console.WriteLine(chinchillaToRemove.ToString());
            context.Chinchillas.Remove(chinchillaToRemove);
            context.SaveChanges();


            //add new chinchillas
            var tmp = new Chinchilla { chi_Name = names[random.Next(names.Length)] };
            context.Chinchillas.Add(tmp);
            context.SaveChanges();


            //get chinchillas
            var chinchillas = context.Chinchillas.ToList();
            string chinchillasJson = Newtonsoft.Json.JsonConvert.SerializeObject(chinchillas);
            Console.WriteLine(chinchillasJson);


            //owners count 
            Console.WriteLine("Owners count\n"+context.ChinchillaOwners.Count());
        }
    }
}
