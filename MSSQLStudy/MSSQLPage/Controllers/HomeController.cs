using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using MSSQLPage.Models;
using MSSQLService;


namespace MSSQLPage.Controllers;


public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly MSSQLServiceManager _sqlService;


    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
        _sqlService = 
            new($@"Server=DESKTOP-IKGQD7V\SQLEXPRESS;Database=ChinchillaShelter;User Id=sa;Password=franek111;TrustServerCertificate=True;");
    }


    public IActionResult Index()
    {
        List<Chinchilla> chinchillaList = _sqlService.GetChinchillaList(out string mess);
        return View(chinchillaList);
    }


    public IActionResult Privacy()
    {
        return View();
    }
    

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
