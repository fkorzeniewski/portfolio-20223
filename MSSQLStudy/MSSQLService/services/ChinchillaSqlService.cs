namespace MSSQLService;


public class ChinchillaSqlService : MSSQLServiceBase
{
    public ChinchillaSqlService(string connStr, ChinchillaShelterDBContext context) 
        : base(connStr, context)
    {
    }


    public List<Chinchilla> GetChinchillas(out string mess)
    {
        if(_context.Chinchillas.ToList() == null)
        {
            mess = "Chinchila entity is null";
            return new List<Chinchilla>();
        }


        var retVal = _context.Chinchillas.ToList();
        mess = $"Dowloaded {retVal.Count}";
        return retVal;
    }
}