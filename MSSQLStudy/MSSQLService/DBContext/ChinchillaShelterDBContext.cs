using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;


namespace MSSQLService;


public class ChinchillaShelterDBContext : DbContext
{
    private readonly string _connStr;
    public DbSet<Chinchilla> Chinchillas { get; set; }


    public ChinchillaShelterDBContext(string connStr)=>
        _connStr = connStr;


    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
        optionsBuilder.UseSqlServer(_connStr);
}