namespace MSSQLService;


public abstract class MSSQLServiceManagerBase : IMSSQLServiceManager
{
    protected string _connStr{get;}


    public MSSQLServiceManagerBase(string connStr) =>
        _connStr = connStr;


    public abstract bool IsConnected();
}