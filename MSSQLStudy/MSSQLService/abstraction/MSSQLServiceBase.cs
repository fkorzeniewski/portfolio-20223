namespace MSSQLService;


public abstract class MSSQLServiceBase
{
    protected string _connStr;
    protected ChinchillaShelterDBContext _context;


    public MSSQLServiceBase(string connStr, ChinchillaShelterDBContext context)
    {
        _connStr = connStr;
        _context = context;
    }
}