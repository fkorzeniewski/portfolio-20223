using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;


namespace MSSQLService;


public class Chinchilla
{
    [Key]
    public int chi_ID { get; set; }
    public string? chi_Name { get; set;}


    public override string ToString()=>
        Newtonsoft.Json.JsonConvert.SerializeObject(this);
}