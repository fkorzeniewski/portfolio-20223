﻿namespace MSSQLService;


public class MSSQLServiceManager : MSSQLServiceManagerBase
{
    private readonly ChinchillaShelterDBContext _context;
    private readonly ChinchillaSqlService _chinchillaSql;
    

    public MSSQLServiceManager(string connStr) : base(connStr)
    {
        _context = new ChinchillaShelterDBContext(connStr);
        _chinchillaSql = new ChinchillaSqlService(connStr, _context);
    }
    

    public override bool IsConnected() => 
        _context.Database.CanConnect();


    public List<Chinchilla> GetChinchillaList(out string mess) =>
        _chinchillaSql.GetChinchillas(out mess);
}
